<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf {

	protected $ci;

    public function __construct()
    {
        $this->ci =& get_instance();
    }

    public function order_invoice($id = 0, $redirect = true)
    {
    	$order = $this->ci->Orders
	                    ->find()
	                    ->where('id', $id)
	                    ->get()
	                    ->row_array();

        if(!$order)
        {
            $this->ci->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('orders'));
        }

        if($order['invoice_file'])
        {
        	redirect(assets('pdf/' . $order['invoice_file']));
        }

        $invoice_no = date('Y') . $order['id'];

        $address = $this->ci->OrderAddress
	                        ->find()
	                        ->where('order_id', $id)
	                        ->get()
	                        ->result_array();

        $products = $this->ci->OrderProducts
	                        ->find()
	                        ->where('order_id', $id)
	                        ->get()
	                        ->result_array();

        $html = $this->ci->load->view('pdf/invoice', compact('order', 'address', 'products', 'invoice_no'), true);

        $filename = 'invoice-' . $invoice_no . '.pdf';
        $filepath = APPPATH . '../assets/pdf/' . $filename;

        $this->ci->Orders->update(['invoice_file' => $filename], $id, false);

        $mpdf = new \Mpdf\Mpdf();
		$mpdf->WriteHTML($html);
		$mpdf->Output($filepath);

		if($redirect)
        {
            redirect(assets('pdf/' . $filename));    
        }
        else
        {
            return assets('pdf/' . $filename);
        }
    }

    public function packing_slip($id = 0, $redirect = true)
    {
    	$order = $this->ci->Orders
	                    ->find()
	                    ->where('id', $id)
	                    ->get()
	                    ->row_array();

        if(!$order)
        {
            $this->ci->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('orders'));
        }

        if($order['packing_slip_file'])
        {
        	redirect(assets('pdf/' . $order['packing_slip_file']));
        }

        $invoice_no = date('Y') . $order['id'];

        $address = $this->ci->OrderAddress
	                        ->find()
	                        ->where('order_id', $id)
	                        ->get()
	                        ->result_array();

        $products = $this->ci->OrderProducts
	                        ->find()
	                        ->where('order_id', $id)
	                        ->get()
	                        ->result_array();

        $html = $this->ci->load->view('pdf/packing_slip', compact('order', 'address', 'products', 'invoice_no'), true);

        $filename = 'packingslip-' . $invoice_no . '.pdf';
        $filepath = APPPATH . '../assets/pdf/' . $filename;

        $this->ci->Orders->update(['packing_slip_file' => $filename], $id, false);

        $mpdf = new \Mpdf\Mpdf();
		$mpdf->WriteHTML($html);
		$mpdf->Output($filepath);

        if($redirect)
        {
            redirect(assets('pdf/' . $filename));    
        }
		else
        {
            return assets('pdf/' . $filename);
        }
    }
}