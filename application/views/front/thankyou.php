<div id="main" class="section">
    <div class="container">
    	<nav class="checkout-breadcrumbs">
		   	<a href="<?= cart_url() ?>" class="current step-cart hide-for-small">
		   		<span class="checkout-name">Your order</span>
		   		<span class="checkout-step"><span class="checkout-counter">1</span><span class="checkout-line"></span></span>
		   	</a>
		   	<a href="<?= checkout_url() ?>" class="current step-cart hide-for-small">
		   		<span class="checkout-name">
Your data</span>
		   		<span class="checkout-step"><span class="checkout-counter">2</span><span class="checkout-line"></span></span>
		   	</a>
		   	<a href="#" class="step-checkout current last">
		   		<span class="checkout-name">Order Complete!</span>
		   		<span class="checkout-step"><span class="checkout-counter">3</span><span class="checkout-line"></span></span>
		   	</a>
	    </nav>

	    <?= prepare_content($page['content']) ?>
    </div>
</div>