<?php
$cart   = $this->session->userdata('cart');
$cartProducts = array();
if($cart && count($cart) > 0){
    foreach ($cart as $k => $v){
        $cartProducts[$v['product']] = $v['quantity'];
    }
}
?>
<div class="col-lg-4 col-sm-6 col-12 my-4 single-pro-div">
    <div class="img_wrapper">
        <?php if($pro['sale_price'] && $pro['sale_price'] > 0) { ?>
        <span class="sale bg_pink text-white d-flex align-items-center justify-content-center">Sale!</span>
        <?php } ?>
        <a href="<?= url('/product/' . $pro['slug']) ?>">
            <img src="<?= $pro['image'] ?>" class="img_def img-fluid m-auto" />
            <!--<img src="<?= $pro['sec_image'] ?>" class="img_hover img-fluid m-auto" />-->
        </a>
        <a href="<?= url('/product/' . $pro['slug']) ?>" class="cart_kopen bg_pink text-white d-flex align-items-center justify-content-between">
            <span class="cart_text">
BUY NOW</span><i class="fas fa-shopping-cart"></i>
        </a>
        <?php if(isset($wishlist_page) && $wishlist_page) { ?>
        <a href="javascript:void(0)" class="remove_wishlist d-flex align-items-center justify-content-between" data-id="<?= $pro['id'] ?>">
            <i class="fas fa-times"></i>
        </a>
        <?php } else { ?>
        <a href="javascript:void(0)" class="wishlist_btn d-flex align-items-center justify-content-between wishlist-icon" data-id="<?= $pro['id'] ?>">

            <i class="<?= in_array($pro['id'], $this->wishlist) ? 'fas' : 'far' ?> fa-heart"></i>
        </a>
        <?php } ?>

        <div class="number">
             <span class="minus" id="<?php echo $pro['id'] ?>">-</span>
            <input type="text" value="<?php echo isset($cartProducts[$pro['id']]) ? $cartProducts[$pro['id']] : 0; ?>"/>
            <span class="plus" id="<?php echo $pro['id'] ?>">+</span>
        </div>
    </div>
    <div class="description text-center mt-3">
        <a href="<?= url('/product/' . $pro['slug']) ?>" class="title text_black">
            <?= $pro['name'] ?>
        </a>
        <div class="price mt-1">
            <?php if($pro['sale_price'] && $pro['sale_price'] > 0) { ?>
            <del class="text_gray"><?= format_money($pro['price']) ?></del>
            <span class="text_black"><?= format_money($pro['sale_price']) ?></span>
            <?php } else { ?>
            <span class="text_black"><?= format_money($pro['price']) ?></span>
            <?php } ?>
        </div>
    </div>
</div>
