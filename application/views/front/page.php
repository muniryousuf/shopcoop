

<section class="home-sec-1">
    <div class="row">
        <div class="col-sm-12">
            <a href=""><img class="banner" src="<?php echo base_url() ?>assets/images/grocery-banner.jpg"></a>
        </div>
    </div>
</section>

<section class="home-sec-2">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h3 class="box-shadow">Grocery delivery and collection</h3>
            </div>
            <div class="col-sm-12 icon-text">
                <i class="fas fa-check"></i>
                <span class="check-text">Delivery or collection within 2 hours</span><br>
                 <i class="fas fa-check"></i>
                <span class="check-text">Picked with care by our store team</span><br>
                 <i class="fas fa-check"></i>
                <span class="check-text">Shop without leaving the house</span><br>

            </div>
            <div class="col-sm-12 banner-image-2">
                <a href=""><img class="banner" src="<?php echo base_url() ?>assets/images/grocery-banne-main.jpg"></a>
            </div>
        </div>
    </div>
</section>



<?= prepare_content($page['content']) ?>


<section class="home-sec-5">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4>Top Aisles</h4>
                <ul>
                    <?php $categories = $this->ProCategories->getAllCategories(); ?>
                    <?php foreach ($categories as $value) { ?>
                    <li><a href="<?php echo url('/product-category/' . $value['slug']) ?>"><?php echo $value['name'] ?></a></li>
                    <?php } ?>

                </ul>
            </div>
        </div>
    </div>
</section>

<section class="home-sec-6">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h3>How it works</h3>
                <p><span>1</span>Select a delivery or collection slot that suits you.</p>
                <p><span>2</span>Fill your basket with our wide range of products from your local store. You need to spend a minimum of £15.</p>
                <p><span>3</span>We’ll pack your shopping for you.</p>
                <br>
                <a href="">See our terms of service</a>
                <span class="faq-details">for full details.</span>
            </div>
        </div>
    </div>
</section>

<section class="home-sec-7">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h2>Reasons to shop with us</h2>
                <br>
            </div>
            <div class="col-sm-4">
                <div class=" color-box">
                    <p>lorem ipsum</p>
                    <p>lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum</p>
                    <a href="#"><button class="become-member-bt">lorem ipsumlorem ipsumlorem ipsumlorem ipsumlorem ipsum</button></a>
                    <br><br>
                    <a href="">Lorem ipsum</a>
                    <img class="banner" src="https://i.ibb.co/7GyDSND/sonder-quest-5ze1ouvz-SM8-unsplash.jpg">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="color-box2">
                    <span>lorem ipsum</span>
                    <h3>lorem ipsum</h3>
                    <img class="banner" src="https://i.ibb.co/7GyDSND/sonder-quest-5ze1ouvz-SM8-unsplash.jpg">
                 </div>
            </div>
            <div class="col-sm-4">
                <div class="color-box3">
                    <span>100% British</span>
                    <h3>lorem ipsumlorem ipsum</h3>
                    <img class="banner" src="https://i.ibb.co/7GyDSND/sonder-quest-5ze1ouvz-SM8-unsplash.jpg">
                </div>
            </div>
        </div>
    </div>
</section>
