<div id="main" class="section">
    <div class="container">


        <form id="frm-checkout" action="<?= url('/process-order') ?>" method="post">
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-12 col-md-6 form-group">
                            <label for="billing_first_name">First Name <span class="required">*</span></label>
                            <input type="text" id="billing_first_name" name="billing[first_name]" class="form-control" value="" maxlength="50" required />
                        </div>
                        <div class="col-12 col-md-6 form-group">
                            <label for="billing_last_name">
                                Last name <span class="required">*</span></label>
                            <input type="text" id="billing_last_name" name="billing[last_name]" class="form-control" value="" maxlength="50" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="billing_country">Country <span class="required">*</span></label>
                        <input type="text" id="billing_country" class="form-control" value="UK" readonly />
                    </div>

                    <div class="form-group">
                        <label for="billing_address1">
                            Street and house number <span class="required">*</span></label>
                        <input type="text" id="billing_address1" name="billing[address1]" class="form-control" placeholder="Street name and house number" value="" maxlength="255" required /><br>
                        <input type="text" id="billing_address2" name="billing[address2]" class="form-control mt-2" placeholder="Appartment, suite, unit etc. (optional)" maxlength="255" value="" />
                    </div>

                    <div class="form-group">
                        <label for="billing_postalcode">Postcode <span class="required">*</span></label>
                        <input type="text" readonly   id="billing_postalcode" name="billing[postalcode]" class="form-control" value="" maxlength="10" required />
                    </div>

                    <div class="form-group">
                        <label for="billing_city">
                            Place <span class="required">*</span></label>
                        <input type="text" id="billing_city" name="billing[city]" class="form-control" value="" maxlength="50" required />
                    </div>

                    <div class="form-group">
                        <label for="phone">
                            Telephone (optional)</label>
                        <input type="text" id="phone" name="phone" class="form-control" maxlength="50" value="" />
                    </div>

                    <div class="form-group">
                        <label for="email">
                            E-mail address<span class="required">*</span></label>
                        <input type="email" id="email" name="email" class="form-control" value="" maxlength="50" required />
                    </div>

                    <div class="form-group">
                        <label for="">Select Day</label>
                            <select class="custom-select form-control" id="select_day">
                              <option selected value="null">Select Day</option>
                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                              <option value="<?php  date('d/m/Y', strtotime('+'.$i.' day')) ?>"><?php echo date('d/m/Y', strtotime('+'.$i.' day')) ?></option>
                                <?php } ?>
                            </select>
                    </div>

                    <div class="form-group" id="select_time">
                        <label for="">Select Time</label>
                            <select class="custom-select form-control">
                              <option selected>Select Time</option>
                              <option value="6pm to 7pm" id="1">6pm to 7pm</option>
                              <option value="7pm to 8pm" id="2">7pm to 8pm</option>
                              <option value="8pm to 9pm" id="3">8pm to 9pm</option>
                            </select>
                    </div>

                    <div class="form-group checkboxes mt-5 mb-4">
                        <label class="text-uppercase">
                            <input type="checkbox" id="create_account" name="create_account" value="1" />
                            <i class="fas fa-user-circle"></i> <strong>
                                Create an account?</strong>
                        </label>
                    </div>

                    <div id="password-div" class="form-group" style="display: none;">
                        <label for="account_password">
                            Create account password<span class="required">*</span></label>
                        <input type="password" id="account_password" name="account_password" class="form-control" value="" minlength="8" required />
                    </div>

                    <div class="form-group checkboxes mt-4 mb-5">
                        <label class="text-uppercase">
                            <input type="checkbox" id="new_shipping_address" name="new_shipping_address" value="1" />
                            <i class="fas fa-truck fa-flip-horizontal"></i> <strong>
                                Different delivery address?</strong>
                        </label>
                    </div>

                    <div id="shipping-div" style="display: none;">
                        <div class="row">
                            <div class="col-12 col-md-6 form-group">
                                <label for="shipping_first_name">First Name
                                    <span class="required">*</span></label>
                                <input type="text" id="shipping_first_name" name="shipping[first_name]" class="form-control" value="" maxlength="50" required />
                            </div>
                            <div class="col-12 col-md-6 form-group">
                                <label for="shipping_last_name">
                                    Last name <span class="required">*</span></label>
                                <input type="text" id="shipping_last_name" name="shipping[last_name]" class="form-control" value="" maxlength="50" required />
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="shipping_country">
                                Country <span class="required">*</span></label>
                            <input type="text" id="shipping_country" class="form-control" value="Uk" readonly />
                        </div>

                        <div class="form-group">
                            <label for="shipping_address1">
                                Street and house number<span class="required">*</span></label>
                            <input type="text" id="shipping_address1" name="shipping[address1]" class="form-control" placeholder="Street name and house number" value="" maxlength="255" required />
                            <input type="text" id="shipping_address2" name="shipping[address2]" class="form-control mt-2" placeholder="Apartment, suite, unit etc. (optional)" value="" maxlength="255" />
                        </div>

                        <div class="form-group">
                            <label for="shipping_postalcode">Postcode <span class="required">*</span></label>
                            <input type="text" id="shipping_postalcode" name="shipping[postalcode]" class="form-control" value="" maxlength="10" required />
                        </div>

                        <div class="form-group">
                            <label for="shipping_city">
                                Place <span class="required">*</span></label>
                            <input type="text" id="shipping_city" name="shipping[city]" class="form-control" value="" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="comments">Order notes (optional)</label>
                        <textarea id="comments" name="comments" class="form-control" placeholder="Notes about your order, for example special notes for delivery."></textarea>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="cart-total">
                        <h2>
                            Your order</h2>
                        <table class="table">
                            <tbody>
                            <?php foreach ($cart['items'] as $key => $item) { ?>
                                <tr class="cart-product">
                                    <td>
                                        <?= $item['product_name'] ?>
                                        x <?= $item['quantity'] ?>
                                    </td>
                                    <th>
                                        <?php if($item['sale_price'] && $item['sale_price'] > 0) { ?>
                                            <?= format_money($item['sale_price'] * $item['quantity']) ?>
                                        <?php } else { ?>
                                            <?= format_money($item['price'] * $item['quantity']) ?>
                                        <?php } ?>
                                    </th>
                                </tr>
                            <?php } ?>

                            <tr class="cart-subtotal">
                                <th>Subtotal</th>
                                <td><?= format_money($cart['subtotal']) ?></td>
                            </tr>
                            <tr class="cart-shipping">
                                <th>
                                    shipment</th>
                                <td>
                                    Free Shipping</td>
                            </tr>
                            <?php if(!$cart['tax_included']) { ?>
                                <tr class="cart-tax">
                                    <th><?= $cart['tax_percent'] ?>% VAT</th>
                                    <td><?= format_money($cart['tax']) ?></td>
                                </tr>
                            <?php } ?>
                            <tr class="order-total">
                                <th>Total</th>
                                <td>
                                    <strong><?= format_money($cart['nettotal']) ?></strong>
                                    <?php if($cart['tax_included']) { ?>
                                        <small class="includes_tax">(including <strong><?= format_money($cart['tax']) ?></strong> <?= $cart['tax_percent'] ?>% VAT)</small>
                                    <?php } ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>

                        <div class="payment-methods">
<!--                            --><?php //foreach (payment_methods() as $k => $pay) { ?>
                                <div class="single-payment-method">
                                    <label>
                                        <input type="radio" name="payment_method" class="payment-method" value="1"  />
                                         Paypal
<!--                                        <img src="--><?//= $pay['image'] ?><!--" />-->
                                    </label>

                                </div>
<!--                            --><?php //} ?>
                        </div>

                        <p class="privacy-text">

                            Your personal data is used to process your order and improve your experience on this website and it is described in us <a href="<?= url('/privacy-policy') ?>" target="_blank">
                                privacy policy</a>.
                        </p>

                        <p class="term-checkbox mb-4">
                            <label>
                                <input type="checkbox" name="terms" id="terms" value="1" required />

                                I have the <a href="<?= url('/terms-and-conditions') ?>" target="_blank">
                                    read and agree to the general terms and conditions </a> of the website <span class="required">*</span>
                            </label>
                        </p>

                        <p>
                            <button type="submit" class="place-order d-block theme_btn text-uppercase text-center" id="place_order">
                                Place an order</button>
                        </p>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

<div id="main" class="section">
    <div class="container">
        <?= prepare_content($page['content']) ?>
    </div>
</div>


<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>

<script>

$(function() {

    if (sessionStorage.getItem("postal_code") === null) {
        localStorage.removeItem('selected_product');
        $('.featuredProductsModal .close').css('display', 'none');
        $('.featuredProductsModal').modal({
            backdrop: 'static',
            keyboard: false,
        });
        $('.featuredProductsModal').modal('show');
    }else {
        $('#billing_postalcode').val(sessionStorage.getItem("postal_code"))
    }

    $('#select_time').hide();
    $('#place_order').addClass('pointer-none')
    $('#select_day').change(function() {

        if ($('#select_day').val() == 'null') {
            $('#select_time').hide();
            $('#place_order').addClass('pointer-none')

        } else {
            $('#select_time').show()
            $("#select_time").change(function() {
                $('#place_order').removeClass('pointer-none')
            });
        }

    });
});

</script>
