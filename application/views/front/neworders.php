<div id="main" class="section">
    <div class="container">
    	<div class="title-wrap text-center">
    		<h1 class="page-title"><?= $page['title'] ?></h1>

			<ol class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?= url('/') ?>">Home</a>
				</li>
				<li class="current"><?= $page['title'] ?></li>
			</ol>
    	</div>

        <?= prepare_content($page['content']) ?>

        <?php if($orders) { ?>
		<?php $status = order_statuses('dt'); ?>
		<div class="table-responsive">
			<table class="table table-striped">
				<thead>
					<tr>
						<th width="10%" class="text-uppercase">Order</th>
						<th class="text-uppercase">Date</th>
						<th class="text-uppercase">Status</th>
						<th class="text-uppercase">Total</th>
						<th width="10%" class="text-uppercase">Actions</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ($orders as $key => $o) { ?>
					<tr>
						<td><a href="<?= account_url() . '?view-order&reference='. array_search($o['id'], $newOrders) ?>">#<?=  $key+1 ?></a></td>
						<td><?= date('d M Y', strtotime($o['created_at'])) ?></td>
						<td><?= isset($status[$o['status']]) ? $status[$o['status']] : 'N/A' ?></td>
						<td><?= format_money($o['net_total']) ?></td>
						<td><a href="<?= account_url() . '?view-order&reference='. array_search($o['id'], $newOrders) ?>" class="text-uppercase theme_btn">See</a></td>
					<?php } ?>		
				</tbody>
			</table>
		</div>
		<?php } else { ?>
		<p>No order placed yet.</p>
		<p><a href="<?= shop_url() ?>" class="theme_btn text-uppercase">
		        Search through products</a></p>
		<?php } ?>
    </div>
</div>