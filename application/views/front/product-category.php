<div id="main" class="section">
	<div class="row">
		<div class="col-sm-12">
	        <a href="<?php echo base_url('/') ?>">
	            <img class="sub-banner" src="<?php echo base_url() ?>assets/images/category-banner.jpg"></a>
	    </div>
    </div>
    <div class="container product-category-detail">
    	
    	<div class="title-wrap text-center">
    		<h1 class="page-title"><?= $category['name'] ?></h1>

			<ol class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
				<li typeof="v:Breadcrumb">
					<a rel="v:url" property="v:title" href="<?= url('/') ?>">Home</a>
				</li>
				<li class="current"><?= $category['name'] ?></li>
			</ol>
    	</div>

        <?= prepare_content($category['description']) ?>
        <div id="supergave">
		    <div class="row sale_wrapper py-5">
		    	<?php
		    	foreach ($products as $pro) {
		        	include APPPATH . 'views/front/partials/single-product.php';
		    	}
		    	?>
		    </div>
		</div>
    </div>
</div>