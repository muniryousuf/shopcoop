<?php
$today = strtotime(date("Y-m-d"));
$productPrice = ($product['sale_price'] > 0 && ($product['sale_start'] > 0) && ($today >= strtotime($product['sale_start'])) && ($today <= strtotime($product['sale_end']))) ? $product['sale_price'] : $product['price'];
$quantity = 1;
?>
<style type="text/css">
    label.checked {
        padding-right: 20px;
        background-color: #fce83f;
    }
</style>


<div id="main" class="section">
    <div class="container">
        <div class="title-wrap text-center">
            <h1 class="page-title"><?= $product['name'] ?></h1>

            <ul class="breadcrumbs" xmlns:v="http://rdf.data-vocabulary.org/#">
                <li typeof="v:Breadcrumb">
                    <a rel="v:url" property="v:title" href="<?= url('/') ?>">Home</a>
                </li>
                <?php if ($categories[0]) { ?>
                    <li typeof="v:Breadcrumb">
                        <a rel="v:url" property="v:title"
                           href="<?= url('/product-category/' . $categories[0]['slug']) ?>"><?= $categories[0]['name'] ?></a>
                    </li>
                <?php } ?>
                <li class="current"><?= $product['name'] ?></li>
            </ul>
        </div>
    </div>
</div>

<section class="shop-sec-1 container">
    <div class="row order-product">
         <div class="col-md-7 order-2 order-md-1 shade order-product-detail">
            <div class="box-shade">


               <!--  <p class="sku">SKU#
                    <span class="sku"><?= $product['sku'] ? $product['sku'] : 'N/A' ?>   </span>
                </p> -->

                <h2 class="page-title"><?= $product['name'] ?></h2>

                        <div class="tagged_as mb-2">Tags:
                                <?php foreach (explode(',', $product['tags']) as $tag) { ?>
                                    <a class="text_gray"
                                       href="<?= url('/product-tag/' . str_replace(' ', '-', $tag)) ?>"><?= $tag ?></a>
                                <?php } ?>
                          </div>


                <h3 class="price">

                    <?php if ($product['sale_price'] > 0 && ($product['sale_start'] > 0) && ($today >= strtotime($product['sale_start'])) && ($today <= strtotime($product['sale_end']))) { ?>
                        <?= format_money($product['price']) ?>
                        <?= format_money($product['sale_price']) ?>
                    <?php } else { ?>
                        <?= format_money($product['price']) ?>
                    <?php } ?>

                </h3>

                <p id="price" class="price">
                    <?php if ($product['sale_price'] > 0 && ($product['sale_start'] > 0) && ($today >= strtotime($product['sale_start'])) && ($today <= strtotime($product['sale_end']))) { ?>
                        <del class="text_gray"><?= format_money($product['price']) ?></del>
                        <span class="text_black"><?= format_money($product['sale_price']) ?></span>
                    <?php } else { ?>
                        <span class="text_black"><?= format_money($product['price']) ?></span>
                    <?php } ?>
                </p>


                    <form id="frm-addtocart" action="<?= url('/cart/add') ?>" method="POST" class="d-none d-md-block frm-check">
                        <input type="hidden" name="action" value="addtocart" />
                        <input type="hidden" name="product" value="<?= $product['id'] ?>" />
                    <?php
                    foreach ($pro_discs ?: array() as $key => $Discount) {
                        $quantityPrice = $productPrice * $Discount['quantity'];
                        $savedPrice = $quantityPrice - $Discount['amount'];
                        $search = ['{quantity}', '{product name}', '{amount}'];
                        $replace = [$Discount['quantity'], $product['name'], '<del class="text_gray">' . format_money($quantityPrice) . '</del> ' . format_money($Discount['amount']) . " and save " . format_money($savedPrice)];
                        $quantity = ($Discount['is_default'] == 1) ? $Discount['quantity'] : $quantity;

                        ?>
                        <div class="p-check">

                            <label class="<?= ($Discount['is_default'] == 1) ? "checked" : "" ?>">
                                <input type="checkbox" class="actQuantityDiscount" name="QuantityDiscountItem"
                                       value="<?= $Discount['id'] ?>" data-amount="<?= $Discount['amount'] ?>"
                                       data-quantity="<?= $Discount['quantity'] ?>" <?= ($Discount['is_default'] == 1) ? "checked" : "" ?>>
                                <span>
                            <?= str_replace($search, $replace, $Discount['message']) ?></span>
                            </label>
                        </div>

                        <?php
                    }
                    ?>

                    <p class="price">Categories<span style="font-size: 14px">
                        <?php foreach ($categories as $cat) { ?>
                            <a class="text_gray text_deco_hover"
                               href="<?= url('/product-category/' . $cat['slug']) ?>"><?= $cat['name'] ?></a>
                            &nbsp;
                        <?php } ?></span></p>


                    <div id="cart-btn-div" class="form-row product-quantity">
                        <div class="counter col-md-3">
                            <div class="px-0 b-l quant_btn">
                                <button type="button" class="btn btn-minus form_elements text-center">-</button>
                            </div>
                            <div class="form-group m-0">
                                <label for="inputQuantity" class="sr-only">Quantity</label>
                                <input type="text" name="quantity" value="<?= $quantity ?>" min="1"
                                       class="form-control form_elements text-center numberonly quantity">
                            </div>
                            <div class="px-0 b-r quant_btn">
                                <button type="button" class="btn btn-plus form_elements text-center">+</button>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <button data-toggle="modal" data-target="#exampleModal"  id="btn-addtocart" type="button" class="theme_btn custom-btn2">Order now!
                            </button>

                              </div>
                    </div>

                    <!--                    <div class="addtocart-selector small">-->
                    <!--                        <label>QTY</label>-->
                    <!--                        <div class="addtocart-qty">-->
                    <!--                            <div class="addtocart-button button-down"><span class="glyphicon glyphicon-minus"-->
                    <!--                                                                            aria-label="increase quantity"></span></div>-->
                    <!--                            <input type="text" class="addtocart-input" value="1"/>-->
                    <!--                            <div class="addtocart-button button-up">-->
                    <!--                                <span class="glyphicon glyphicon-plus" aria-label="increase quantity"></span>-->
                    <!--                            </div>-->
                    <!--                        </div>-->
                    <!--                        <button>ADD TO CART</button>-->
                    <!--                    </div>-->

                    <div class="share_icon pt-3">
                        <p class="mb-2"><strong class="text_black">Share this product</strong></p>
                        <a href="http://www.facebook.com/sharer.php?u=<?= urlencode(url('/product/' . $product['slug'])) ?>"
                           class="btn social-media-btn"><i class="fab fa-facebook-f blue"></i></a>
                    </div>


                    <!--                <h4 style="font-size: 20px; font-weight: 700;">✅ Expected Lead Time for Delivery / Click & Collect : 3-5-->
                    <!--                    Working Days</h4>-->

                    <div class="tab-content py-3 px-sm-0" id="nav-tabContent">
                        <div class="tab-pane fade show active" id="nav-beschrijving" role="tabpanel"
                             aria-labelledby="nav-beschrijving-tab">

                        </div>
                        <div class="tab-pane fade" id="nav-extra-informatie" role="tabpanel"
                             aria-labelledby="nav-extra-informatie-tab">

                            <?php

                            if (isset($attribute_vals) && count($attribute_vals) > 0) {

                                foreach ($attribute_vals as $av) { ?>
                                    <div class="row info_box px-2 py-4">
                                        <strong class="col-lg-2 pl-lg-4"><?= $av['name'] ?></strong>
                                        <span class="col-lg-9"><?= implode(', ', $av['childs']) ?></span>
                                    </div>
                                <?php }
                            } ?>
                        </div>
                    </div>

            </div>
        </div>
        <div class="col-md-5 order-1 order-md-2 order-product-gallery">
            <div id="procuctCarousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#procuctCarousel" data-slide-to="0" class="active">
                        <img src="<?= uploaded_url($product['image']) ?>" class="img-fluid"/>
                    </li>

                    <?php if(count($images) > 0) { ?>
                    <?php foreach ($images as $k => $img) { ?>
                        <li data-target="#procuctCarousel" data-slide-to="<?= $k + 1 ?>">
                            <img src="<?= resize_img($img['image'], 200, 200) ?>" class="img-fluid"/>
                        </li>
                    <?php } }?>

                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <?php if ($product['sale_price'] > 0 && ($product['sale_start'] > 0) && ($today >= strtotime($product['sale_start'])) && ($today <= strtotime($product['sale_end']))) { ?>
                            <span class="sale bg_pink text-white d-flex align-items-center justify-content-center">Sale!</span>
                        <?php } ?>
                        <a href="javascript:void(0)"
                           class="wishlist_btn d-flex align-items-center justify-content-between"
                           data-id="<?= $product['id'] ?>">
                            <i class="<?= in_array($product['id'], $this->wishlist) ? 'fas' : 'far' ?> fa-heart"></i>
                        </a>

                        <img src="<?= uploaded_url($product['image']) ?>" class="img-fluid"/>
                    </div>
                    <?php foreach ($images as $k => $img) { ?>
                        <div class="carousel-item img_wrapper" data-slide-number="<?= $k + 1 ?>">
                            <?php if ($product['sale_price'] > 0 && ($product['sale_start'] > 0) && ($today >= strtotime($product['sale_start'])) && ($today <= strtotime($product['sale_end']))) { ?>
                                <span class="sale bg_pink text-white d-flex align-items-center justify-content-center">Sale!</span>
                            <?php } ?>
                            <img src="<?= uploaded_url($img['image']) ?>" class="img-fluid"/>
                        </div>
                    <?php } ?>
                    <!-- <div class="carousel-item">
                      <img class="d-block w-100" src="..." alt="Third slide">
                    </div> -->
                </div>
                <a class="carousel-control-prev" href="#procuctCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#procuctCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <!--            <img src="--><?php //echo base_url() ?><!--assets/images/ab000500_1_1.jpg">-->
        </div>

    </div>
    <br>
</section>


<!-- Product Information -->

<section class="container">
    <?= prepare_content($product['description']) ?>

</section>
<!-- Product Information end -->


<section id="product" class="product-detail">
    <div class="container">
        <div class="row pt-3 pb-5">
            <div class="col-md-5">
                <!--div id="procuctCarousel" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#procuctCarousel" data-slide-to="0" class="active">
                            <img src="<? /*= uploaded_url($product['image']) */ ?>" class="img-fluid"/>
                        </li>
                        <?php /*foreach ($images as $k => $img) { */ ?>
                            <li data-target="#procuctCarousel" data-slide-to="<? /*= $k + 1 */ ?>">
                                <img src="<? /*= resize_img($img['image'], 200, 200) */ ?>" class="img-fluid"/>
                            </li>
                        <?php /*} */ ?>

                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <?php /*if ($product['sale_price'] > 0 && ($product['sale_start'] > 0) && ($today >= strtotime($product['sale_start'])) && ($today <= strtotime($product['sale_end']))) { */ ?>
                                <span class="sale bg_pink text-white d-flex align-items-center justify-content-center">Sale!</span>
                            <?php /*} */ ?>
                            <a href="javascript:void(0)"
                               class="wishlist_btn d-flex align-items-center justify-content-between"
                               data-id="<? /*= $product['id'] */ ?>">
                                <i class="<? /*= in_array($product['id'], $this->wishlist) ? 'fas' : 'far' */ ?> fa-heart"></i>
                            </a>

                            <img src="<? /*= uploaded_url($product['image']) */ ?>" class="img-fluid"/>
                        </div>
                        <?php /*foreach ($images as $k => $img) { */ ?>
                            <div class="carousel-item img_wrapper" data-slide-number="<? /*= $k + 1 */ ?>">
                                <?php /*if ($product['sale_price'] > 0 && ($product['sale_start'] > 0) && ($today >= strtotime($product['sale_start'])) && ($today <= strtotime($product['sale_end']))) { */ ?>
                                    <span class="sale bg_pink text-white d-flex align-items-center justify-content-center">Sale!</span>
                                <?php /*} */ ?>
                                <img src="<? /*= uploaded_url($img['image']) */ ?>" class="img-fluid"/>
                            </div>
                        <?php /*} */ ?>
                         <div class="carousel-item">
                          <img class="d-block w-100" src="..." alt="Third slide">
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#procuctCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#procuctCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>-->

            </div>

           <!-- <div class="col-md-7">
                <div class="pro-d">
                    <p id="price" class="border_bottom price">
                        <?php /*if ($product['sale_price'] > 0 && ($product['sale_start'] > 0) && ($today >= strtotime($product['sale_start'])) && ($today <= strtotime($product['sale_end']))) { */?>
                            <del class="text_gray"><?/*= format_money($product['price']) */?></del>
                            <span class="text_black"><?/*= format_money($product['sale_price']) */?></span>
                        <?php /*} else { */?>
                            <span class="text_black"><?/*= format_money($product['price']) */?></span>
                        <?php /*} */?>
                    </p>
                    <form id="frm-addtocart" action="<?/*= url('/cart/add') */?>" method="POST"
                          class="d-none d-md-block frm-check">
                        <?php
/*                        foreach ($pro_discs ?: array() as $key => $Discount) {
                            $quantityPrice = $productPrice * $Discount['quantity'];
                            $savedPrice = $quantityPrice - $Discount['amount'];
                            $search = ['{quantity}', '{product name}', '{amount}'];
                            $replace = [$Discount['quantity'], $product['name'], '<del class="text_gray">' . format_money($quantityPrice) . '</del> ' . format_money($Discount['amount']) . " and save " . format_money($savedPrice)];
                            $quantity = ($Discount['is_default'] == 1) ? $Discount['quantity'] : $quantity;

                            */?>
                            <div class="p-check">

                                <label class="<?/*= ($Discount['is_default'] == 1) ? "checked" : "" */?>">
                                    <input type="checkbox" class="actQuantityDiscount" name="QuantityDiscountItem"
                                           value="<?/*= $Discount['id'] */?>" data-amount="<?/*= $Discount['amount'] */?>"
                                           data-quantity="<?/*= $Discount['quantity'] */?>" <?/*= ($Discount['is_default'] == 1) ? "checked" : "" */?>>
                                    <span>
                            <?/*= str_replace($search, $replace, $Discount['message']) */?></span>
                                </label>
                            </div>

                            <?php
/*                        }
                        */?>

                        <div class="short_description mb-4">
                            <?/*= prepare_content($product['short_description']) */?>
                        </div>


                        <input type="hidden" name="action" value="addtocart"/>
                        <input type="hidden" name="product" value="<?/*= $product['id'] */?>"/>
                        <div class="proAttrDiv">

                            <?php /*if (isset($attribute_vals) && count($attribute_vals) > 0) {
                                if ($attribute_vals) {
                                    for ($a = 0; $a < $quantity; $a++) {
                                        $attrCount = 1;

                                        */?>

                                        <div class="form-row proAttrRow">

                                            <?php /*foreach ($attribute_vals as $k1 => $v1) { */?>
                                                <div class="form-group col-md-6">
                                                    <label for="inputState"><strong><?/*= $v1['name'] . " " . ($a + 1) */?></strong></label>
                                                    <select id="attr<?/*= $k1 */?>" name="attribute[<?/*= $a */?>][<?/*= $k1 */?>]"
                                                            class="form-control form_elements product-attribute"
                                                            data-key="<?/*= $k1 */?>" required>
                                                        <option value="">
                                                            Choose an option
                                                        </option>
                                                        <?php /*foreach ($v1['childs'] as $k2 => $v2) { */?>
                                                            <option value="<?/*= $k2 */?>"><?/*= $v2 */?></option>
                                                        <?php /*} */?>
                                                    </select>
                                                </div>
                                                <?php
/*                                                if ($attrCount == 3) {
                                                    $attrCount = 1;
                                                    echo '</div><div class="form-row proAttrRow">';
                                                } else {
                                                    $attrCount++;
                                                }
                                            }
                                            */?>
                                        </div>
                                        <?php
/*                                    }
                                }
                            }
                            */?>
                        </div>

                        <div id="cart-btn-div" class="form-row product-quantity">
                            <div class="counter col-md-3">
                                <div class="px-0 b-l quant_btn">
                                    <button type="button" class="btn btn-minus form_elements text-center">-</button>
                                </div>
                                <div class="form-group m-0">
                                    <label for="inputQuantity" class="sr-only">Quantity</label>
                                    <input type="text" name="quantity" value="<?/*= $quantity */?>" min="1"
                                           class="form-control form_elements text-center numberonly quantity">
                                </div>
                                <div class="px-0 b-r quant_btn">
                                    <button type="button" class="btn btn-plus form_elements text-center">+</button>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <button type="button" id="btn-addtocart" class="theme_btn custom-btn2">Order now!
                                </button>
                            </div>
                        </div>

                        <div id="stock-alert-div" style="display: none">
                            <div class="alert alert-danger stock-alert">Product out of stock</div>

                            <div class="card text-center" style="max-width: 25rem;">
                                <div class="card-header">
                                    Email me when stock is available
                                </div>
                                <div class="card-body">
                                    <input id="restock_email" type="email" class="form-control" name="restock_email"
                                           placeholder="E-mailadres"/>
                                    <button id="restock_btn" type="button" class="theme_btn mt-3">Subscribe</button>
                                </div>
                            </div>
                        </div>
                    </form>

                    <div class="product_meta text_gray mt-3">
                    <span class="posted_in">Categories
                    	<?php /*foreach ($categories as $cat) { */?>
                            <a class="text_gray text_deco_hover"
                               href="<?/*= url('/product-category/' . $cat['slug']) */?>"><?/*= $cat['name'] */?></a>
                            &nbsp;
                        <?php /*} */?>
                    </span>

                        <span class="sku_wrapper">SKU:
                        <span class="sku"><?/*= $product['sku'] ? $product['sku'] : 'N/A' */?></span>
                    </span>

                        <div class="tagged_as mt-3">Tags:
                            <?php /*foreach (explode(',', $product['tags']) as $tag) { */?>
                                <a class="text_gray"
                                   href="<?/*= url('/product-tag/' . str_replace(' ', '-', $tag)) */?>"><?/*= $tag */?></a>
                            <?php /*} */?>
                        </div>
                    </div>

                    <div class="share_icon pt-5">
                        <p class="mb-2"><strong class="text_black">Share this product</strong></p>
                        <a href="http://www.facebook.com/sharer.php?u=<?/*= urlencode(url('/product/' . $product['slug'])) */?>"
                           class="btn btn-fb"><i class="fab fa-facebook-f"></i></a>
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</section>

<!-- <section id="mobile-btn-div" class="d-block d-md-none">
    <div class="container">
        <a id="mobile-order-btn" href="javascript:void(0)" class="d-block theme_btn text-center">Order now!</a>
    </div>
</section> -->

<aside id="mobile-order-div" class="d-none">
    <div class="sidebar_close_btn">
        <span></span>
    </div>
    <div class="frm"></div>
</aside>

<!--<section id="product_description">-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-12">-->
<!--                <nav>-->
<!--                    <div class="nav flex-column flex-sm-row" id="nav-tab" role="tablist">-->
<!--                        <a class="nav-item text_gray active" id="nav-beschrijving-tab" data-toggle="tab"-->
<!--                           href="#nav-beschrijving" role="tab" aria-controls="nav-beschrijving" aria-selected="true">DESCRIPTION</a>-->
<!--                        <a class="nav-item text_gray" id="nav-extra-informatie-tab" data-toggle="tab"-->
<!--                           href="#nav-extra-informatie" role="tab" aria-controls="nav-extra-informatie"-->
<!--                           aria-selected="false">-->
<!--                            EXTRA INFORMATION</a>-->
<!--                    </div>-->
<!--                </nav>-->
<!--                <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">-->
<!--                    <div class="tab-pane fade show active" id="nav-beschrijving" role="tabpanel"-->
<!--                         aria-labelledby="nav-beschrijving-tab">-->
<!--                        --><? //= prepare_content($product['description']) ?>
<!--                    </div>-->
<!--                    <div class="tab-pane fade" id="nav-extra-informatie" role="tabpanel"-->
<!--                         aria-labelledby="nav-extra-informatie-tab">-->
<!---->
<!--                        --><?php
//
//                        if (isset($attribute_vals) && count($attribute_vals) > 0) {
//
//                            foreach ($attribute_vals as $av) { ?>
<!--                                <div class="row info_box px-2 py-4">-->
<!--                                    <strong class="col-lg-2 pl-lg-4">--><? //= $av['name'] ?><!--</strong>-->
<!--                                    <span class="col-lg-9">--><? //= implode(', ', $av['childs']) ?><!--</span>-->
<!--                                </div>-->
<!--                            --><?php //}
//                        } ?>
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->

<?php if ($upsell_products) { ?>
    <section id="product_related">
        <div class="container">
            <div class="row pb-5">
                <div class="col-12 divider pb-5">
                    <hr>
                </div>
                <div class="col-12">
                    <h4 class="border_bottom">
                        YOU MAY ALSO LIKE THIS ...</h4>
                </div>
                <?php foreach ($upsell_products as $pro) { ?>
                    <div class="col-lg-4 pb-4">
                        <div class="row px-3 related_product_inner">
                            <div class="col-3 px-0 img_wrapper">
                                <?php if ($pro['sale_price']) { ?>
                                    <span class="percent bg_pink text-white d-flex align-items-center justify-content-center">%</span>
                                <?php } ?>
                                <a href="<?= url('/product/' . $pro['slug']) ?>">
                                    <img src="<?= $pro['image'] ?>" class="img-fluid d-block mx-auto"/>
                                </a>
                            </div>
                            <div class="col-9 pr-0 related_product_description">
                                <h5><a href="<?= url('/product/' . $pro['slug']) ?>"
                                       class="text_black"><?= $pro['name'] ?></a></h5>
                                <p>
                                    <?php if ($pro['sale_price']) { ?>
                                        <del class="text_gray"><?= format_money($pro['price']) ?></del>
                                        <span class="text_black"><?= format_money($pro['sale_price']) ?></span>
                                    <?php } else { ?>
                                        <span class="text_black"><?= format_money($pro['price']) ?></span>
                                    <?php } ?>
                                </p>
                                <div class="btn_wrapper">
                                    <a href="<?= url('/product/' . $pro['slug']) ?>" class="theme_btn text-white">
                                        BUY NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>

<?php if ($related_products) { ?>
    <section id="product_related">
        <div class="container">
            <div class="row pb-5">
                <div class="col-12 divider pb-5">
                    <hr>
                </div>
                <div class="col-12">
                    <h4 class="border_bottom">
                        RELATED PRODUCTS</h4>
                </div>
                <?php foreach ($related_products as $pro) { ?>
                    <div class="col-lg-4 pb-4">
                        <div class="row px-3 related_product_inner">
                            <div class="col-3 px-0 img_wrapper">
                                <?php if ($pro['sale_price']) { ?>
                                    <span class="percent bg_pink text-white d-flex align-items-center justify-content-center">%</span>
                                <?php } ?>
                                <a href="<?= url('/product/' . $pro['slug']) ?>">
                                    <img src="<?= $pro['image'] ?>" class="img-fluid d-block mx-auto"/>
                                </a>
                            </div>
                            <div class="col-9 pr-0 related_product_description">
                                <h5><a href="<?= url('/product/' . $pro['slug']) ?>"
                                       class="text_black"><?= $pro['name'] ?></a></h5>
                                <p>
                                    <?php if ($pro['sale_price']) { ?>
                                        <del class="text_gray"><?= format_money($pro['price']) ?></del>
                                        <span class="text_black"><?= format_money($pro['sale_price']) ?></span>
                                    <?php } else { ?>
                                        <span class="text_black"><?= format_money($pro['price']) ?></span>
                                    <?php } ?>
                                </p>
                                <div class="btn_wrapper">
                                    <a href="<?= url('/product/' . $pro['slug']) ?>" class="theme_btn text-white">
                                        BUY NOW</a>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } ?>

<script type="text/javascript">
</script>

<style>
    button.btn.btn-minus.form_elements.text-center {
        position: absolute;
        top: 0;
        padding: 30px 15px;
    }

    button.btn.btn-plus.form_elements.text-center {
        right: 0;
        position: absolute;
        top: 0;
        padding: 30px 31px;
    }


    .custom-btn2 {

        padding: 20px 40px;
        margin-top: 17px !Important;
    }
    @media screen and (max-width: 812px) {
       button.btn.btn-plus.form_elements.text-center {
           padding: 27px 54px 27px 0;
       } 
    }

</style>


<script type="text/javascript">
    var stock = <?= $product['stock'] != '' ? $product['stock'] : 999 ?>;
    var price = '<?= $product['price'] ? format_money($product['price']) : '' ?>';
    var sale_price = '<?= $product['sale_price'] ? format_money($product['sale_price']) : '' ?>';
    var product_attrs = <?= $attributes ? json_encode($attributes) : '[]' ?>;
    var attributes = <?= $attribute_vals ? json_encode($attribute_vals) : '[]' ?>;

</script>

