<?= '<?xml version="1.0" encoding="UTF-8" ?>' ?>
<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">
    <channel>
        <title>Facebook products feed</title>
        <link><?= url() ?></link>
        <description><?= site_name() ?> Product Feed For Facebook</description>
        <?php foreach($products as $p) { ?>
        <item>
            <g:id><?= $p['aid'] ? $p['aid'] : $p['id'] ?></g:id>
            <g:title><?= $p['name'] ?></g:title>
            <g:description><?= $p['description'] ?></g:description>
            <g:link><?= $p['url'] ?></g:link>
            <g:image_link><?= $p['image'] ?></g:image_link>
            <g:availability><?= $p['stock'] ? 'in stock' : 'out of stock' ?></g:availability>
            <g:price>EUR <?= number_format($p['price'], 2, ",", ".") ?></g:price>
            <g:condition>New</g:condition>
            <?= $p['aid'] ? '<g:item_group_id>' . $p['id'] . '</g:item_group_id>' : '' ?>
            <g:identifier_exists>no</g:identifier_exists>
        </item>
        <?php } ?>
    </channel>
</rss>