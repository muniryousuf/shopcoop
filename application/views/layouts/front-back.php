<section class="home-sec-1 container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <a href=""><img class="banner" src="<?php echo base_url() ?>assets/images/grocery-banne-main.jpg"></a>
        </div>
    </div>

    <div class="row two">
        <div class="col-sm-12">
            <a href=""><p class="text-color">Kitchens</p></a>
        </div>
    </div>
</section>

<section class="home-sec-2 container-fluid">
    <div style="background-image:url('<?php echo base_url() ?>assets/images/cta-background.jpg'); padding: 20px;"
         class="row container-fluid">
        <div class="col-sm-1 text-right">
            <img src="<?php echo base_url() ?>assets/images/compass.png">
        </div>
        <div class="col-sm-3">
            <h3>Free next day delivery</h3>
            <p>For orders over £200</p>
        </div>
        <div class="col-sm-1 text-right">
            <img src="<?php echo base_url() ?>assets/images/basket.png">
        </div>
        <div class="col-sm-3">
            <h3>Click & Collect</h3>
            <p>10,000+ products in stock</p>
        </div>
        <div class="col-sm-1 text-right">
            <img src="<?php echo base_url() ?>assets/images/wallet.png">
        </div>
        <div class="col-sm-3">
            <h3>Honest pricing</h3>
            <p>Low from the start</p>
        </div>
    </div>
</section>


<section class="home-sec-3 container-fluid">
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2>FEATU<span>RED</span> PROD<span>UCTS</span></h2>
        </div>
    </div>

    <!--carousel fedi-->
    <div class="owl-carousel owl-loaded owl-drag">
        <div class="owl-stage-outer">
            <div class="owl-stage"
                 style="transform: translate3d(-4246px, 0px, 0px); transition: all 0.25s ease 0s; width: 100%;">

                <div class="owl-item active ">
                    <div class="item product-box">
                        <a href=""><img src="<?php echo base_url() ?>assets/images/ab000560_1_1.jpg"
                                        class="d-block w-100" alt=""></a>
                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>
                        <p class="text-before">£0.30<span>Excl Vat</span></p>
                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>
                        <a style="padding-left: 25px;" href="">
                            <button>ADD TO CART</button>
                        </a>
                    </div>
                </div>
                <div class="owl-item active ">
                    <div class="item product-box">
                        <a href=""><img src="<?php echo base_url() ?>assets/images/ab000560_1_1.jpg"
                                        class="d-block w-100" alt=""></a>
                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>
                        <p class="text-before">£0.30<span>Excl Vat</span></p>
                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>
                        <a style="padding-left: 25px;" href="">
                            <button>ADD TO CART</button>
                        </a>
                    </div>
                </div>
                <div class="owl-item active ">
                    <div class="item product-box">
                        <a href=""><img src="<?php echo base_url() ?>assets/images/ab000560_1_1.jpg"
                                        class="d-block w-100" alt=""></a>
                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>
                        <p class="text-before">£0.30<span>Excl Vat</span></p>
                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>
                        <a style="padding-left: 25px;" href="">
                            <button>ADD TO CART</button>
                        </a>
                    </div>
                </div>
                <div class="owl-item active ">
                    <div class="item product-box">
                        <a href=""><img src="<?php echo base_url() ?>assets/images/ab000560_1_1.jpg"
                                        class="d-block w-100" alt=""></a>
                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>
                        <p class="text-before">£0.30<span>Excl Vat</span></p>
                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>
                        <a style="padding-left: 25px;" href="">
                            <button>ADD TO CART</button>
                        </a>
                    </div>
                </div>
                <div class="owl-item active ">
                    <div class="item product-box">
                        <a href=""><img src="<?php echo base_url() ?>assets/images/ab000560_1_1.jpg"
                                        class="d-block w-100" alt=""></a>
                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>
                        <p class="text-before">£0.30<span>Excl Vat</span></p>
                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>
                        <a style="padding-left: 25px;" href="">
                            <button>ADD TO CART</button>
                        </a>
                    </div>
                </div>


            </div>
        </div>
        <div class="owl-nav disabled">
            <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button>
            <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
        </div>
        <div class="owl-dots">
            <button role="button" class="owl-dot"><span></span></button>
            <button role="button" class="owl-dot"><span></span></button>
            <button role="button" class="owl-dot active"><span></span></button>
            <button role="button" class="owl-dot"><span></span></button>
        </div>
    </div>

</section>


<section class="home-sec-4 container-fluid">
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2>BE<span>ST</span> SELL<span>ERS</span></h2>
        </div>
    </div>

    <div class="owl-carousel owl-loaded owl-drag">
        <div class="owl-stage-outer">
            <div class="owl-stage"
                 style="transform: translate3d(-4246px, 0px, 0px); transition: all 0.25s ease 0s; width: 100%;">

                <div class="owl-item active ">
                    <div class="item product-box">
                        <a href=""><img src="<?php echo base_url() ?>assets/images/ab000560_1_1.jpg"
                                        class="d-block w-100" alt=""></a>
                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>
                        <p class="text-before">£0.30<span>Excl Vat</span></p>
                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>
                        <a style="padding-left: 25px;" href="">
                            <button>ADD TO CART</button>
                        </a>
                    </div>
                </div>
                <div class="owl-item active ">
                    <div class="item product-box">
                        <a href=""><img src="<?php echo base_url() ?>assets/images/ab000560_1_1.jpg"
                                        class="d-block w-100" alt=""></a>
                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>
                        <p class="text-before">£0.30<span>Excl Vat</span></p>
                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>
                        <a style="padding-left: 25px;" href="">
                            <button>ADD TO CART</button>
                        </a>
                    </div>
                </div>
                <div class="owl-item active ">
                    <div class="item product-box">
                        <a href=""><img src="<?php echo base_url() ?>assets/images/ab000560_1_1.jpg"
                                        class="d-block w-100" alt=""></a>
                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>
                        <p class="text-before">£0.30<span>Excl Vat</span></p>
                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>
                        <a style="padding-left: 25px;" href="">
                            <button>ADD TO CART</button>
                        </a>
                    </div>
                </div>
                <div class="owl-item active ">
                    <div class="item product-box">
                        <a href=""><img src="<?php echo base_url() ?>assets/images/ab000560_1_1.jpg"
                                        class="d-block w-100" alt=""></a>
                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>
                        <p class="text-before">£0.30<span>Excl Vat</span></p>
                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>
                        <a style="padding-left: 25px;" href="">
                            <button>ADD TO CART</button>
                        </a>
                    </div>
                </div>
                <div class="owl-item active ">
                    <div class="item product-box">
                        <a href=""><img src="<?php echo base_url() ?>assets/images/ab000560_1_1.jpg"
                                        class="d-block w-100" alt=""></a>
                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>
                        <p class="text-before">£0.30<span>Excl Vat</span></p>
                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>
                        <a style="padding-left: 25px;" href="">
                            <button>ADD TO CART</button>
                        </a>
                    </div>
                </div>


            </div>
        </div>
        <div class="owl-nav disabled">
            <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button>
            <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
        </div>
        <div class="owl-dots">
            <button role="button" class="owl-dot"><span></span></button>
            <button role="button" class="owl-dot"><span></span></button>
            <button role="button" class="owl-dot active"><span></span></button>
            <button role="button" class="owl-dot"><span></span></button>
        </div>
    </div>


</section>


<!--section-4 product-slider-on-mibile-cloose-->


<section class="home-sec-5 container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <h2>TOP CATEGO<span>RIES</span></h2>
        </div>
    </div>

    <div style="padding-top: 40px;" class="row">
        <div class="col-sm-3">
            <img src="<?php echo base_url() ?>assets/images/brick.jpg">
            <h3>Building Materials</h3>
            <ul class="item-list">
                <li><a href="#">Bricks & Blocks</a></li>
                <li><a href="#">Insulation</a></li>
                <li><a href="#">Cements & Aggregates</a></li>
                <li><a href="#">Roofing & Ventilation</a></li>
            </ul>
        </div>
        <div class="col-sm-3">
            <img src="<?php echo base_url() ?>assets/images/top-category-3.jpg">
            <h3>Timber & Joinery </h3>
            <ul class="item-list">
                <li><a href="#">Timbers</a></li>
                <li><a href="#">Sheet Materials</a></li>
                <li><a href="#">Stairs</a></li>
            </ul>
        </div>
        <div class="col-sm-3">
            <img src="<?php echo base_url() ?>assets/images/plast_1.jpg">
            <h3>Plastering & Drylining</h3>
            <ul class="item-list">
                <li><a href="#">Plasterboards & Coving</a></li>
                <li><a href="#">Plasters & Renders</a></li>
                <li><a href="#">Plasters & Renders</a></li>
                <li><a href="#">Plasters & Render Beads</a></li>
            </ul>
        </div>
        <div class="col-sm-3">
            <img src="<?php echo base_url() ?>assets/images/top-category-2.jpg">
            <h3>Garden & Landscaping</h3>
            <ul class="item-list">
                <li><a href="#">Paving & Slabs</a></li>
                <li><a href="#">Fencing</a></li>
            </ul>
        </div>
    </div>
</section>

