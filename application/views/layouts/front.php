<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title><?= $this->seo['title'] ?></title>
    <meta name="description" content="<?= $this->seo['desc'] ?>">
    <meta name="robots" content="<?= $this->seo['robots'] ? strtoupper($this->seo['robots']) : 'INDEX,FOLLOW' ?>"/>
    <?php if ($this->vars['site']['favicon']) { ?>
        <link rel="icon" href="<?= uploaded_url($this->vars['site']['favicon']) ?>" type="image/png" sizes="16x16"/>
    <?php } ?>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style-front.css">

    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.carousel.min.css">
    <script src="<?php echo base_url() ?>assets/js/owl.carousel.js"></script>

    <!-- Animation CSS -->
    <link rel="stylesheet" href="<?= assets('theme1/css/animate.css?r=210120') ?>">
    <!-- Latest Bootstrap min CSS -->
    <link rel="stylesheet" href="<?= assets('theme1/bootstrap/css/bootstrap.min.css') ?>">
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800,900&display=swap"
          rel="stylesheet">
    <!-- Icon Font CSS -->
    <link rel="stylesheet" href="<?= assets('theme1/css/all.min.css?r=210120') ?>">
    <link rel="stylesheet" href="<?= assets('theme1/css/ionicons.min.css?r=210120') ?>">
    <link rel="stylesheet" href="<?= assets('theme1/css/themify-icons.css?r=210120') ?>">
    <link rel="stylesheet" href="<?= assets('theme1/css/linearicons.css?r=210120') ?>">
    <link rel="stylesheet" href="<?= assets('theme1/css/flaticon.css?r=210120') ?>">
    <link rel="stylesheet" href="<?= assets('theme1/css/simple-line-icons.css?r=210120') ?>">
    <!--- owl carousel CSS-->
    <link rel="stylesheet" href="<?= assets('theme1/owlcarousel/css/owl.carousel.min.css?r=210120') ?>">
    <link rel="stylesheet" href="<?= assets('theme1/owlcarousel/css/owl.theme.css?r=210120') ?>">
    <link rel="stylesheet" href="<?= assets('theme1/owlcarousel/css/owl.theme.default.min.css?r=210120') ?>">
    <!-- Magnific Popup CSS -->
    <link rel="stylesheet" href="<?= assets('theme1/css/magnific-popup.css?r=210120') ?>">
    <!-- Slick CSS -->
    <link rel="stylesheet" href="<?= assets('theme1/css/slick.css?r=210120') ?>">
    <link rel="stylesheet" href="<?= assets('theme1/css/slick-theme.css?r=210120') ?>">
    <!-- Style CSS -->
    <link rel="stylesheet" href="<?= assets('theme1/css/style.css?r=210120') ?>">
    <link rel="stylesheet" href="<?= assets('theme1/css/responsive.css?r=210120') ?>">
    <?= $this->vars['site']['before_head'] ?>
</head>
<body>
<?= $this->vars['site']['after_body'] ?>

<?= $header ?>

<?php
$success = $this->session->flashdata('success');
$errors = $this->session->flashdata('error');

$this->session->set_flashdata('success', '');
$this->session->set_flashdata('error', '');
?>

<?php if ($success) { ?>
    <div class="container pt-5 pb-2">
        <div class="alert alert-success"><?= $success ?></div>
    </div>
<?php } ?>

<?php if ($errors) { ?>
    <div class="container pt-5 pb-2">
        <div class="alert alert-danger"><?= $errors ?></div>
    </div>
<?php } ?>

<?= $body ?>

<?= $footer ?>


<!-- Latest jQuery -->
<script src="<?= assets('theme1/js/jquery-1.12.4.min.js?r=210120') ?>"></script>
<!-- popper min js -->
<script src="<?= assets('theme1/js/popper.min.js?r=210120') ?>"></script>
<!-- Latest compiled and minified Bootstrap -->
<script src="<?= assets('theme1/bootstrap/js/bootstrap.min.js?r=210120') ?>"></script>
<!-- owl-carousel min js  -->
<script src="<?= assets('theme1/owlcarousel/js/owl.carousel.min.js?r=210120') ?>"></script>
<!-- magnific-popup min js  -->
<script src="<?= assets('theme1/js/magnific-popup.min.js?r=210120') ?>"></script>
<!-- waypoints min js  -->
<script src="<?= assets('theme1/js/waypoints.min.js?r=210120') ?>"></script>
<!-- parallax js  -->
<script src="<?= assets('theme1/js/parallax.js?r=210120') ?>"></script>
<!-- countdown js  -->
<script src="<?= assets('theme1/js/jquery.countdown.min.js?r=210120') ?>"></script>
<!-- fit video  -->
<script src="<?= assets('theme1/js/Hoverparallax.min.js?r=210120') ?>"></script>
<!-- imagesloaded js -->
<script src="<?= assets('theme1/js/imagesloaded.pkgd.min.js?r=210120') ?>"></script>
<!-- isotope min js -->
<script src="<?= assets('theme1/js/isotope.min.js?r=210120') ?>"></script>
<!-- jquery.appear js  -->
<script src="<?= assets('theme1/js/jquery.appear.js?r=210120') ?>"></script>
<!-- jquery.dd.min js -->
<script src="<?= assets('theme1/js/jquery.dd.min.js?r=210120') ?>"></script>
<!-- slick js -->
<script src="<?= assets('theme1/js/slick.min.js?r=210120') ?>"></script>
<!-- elevatezoom js -->
<script src="<?= assets('theme1/js/jquery.elevatezoom.js?r=210120') ?>"></script>

<!-- Looped scripts -->
<?php foreach ($this->js as $js) { ?>
    <script src="<?= $js ?>"></script>
<?php } ?>

<!-- scripts js -->


<script src="<?php echo base_url() ?>assets/js/owl.carousel.js"></script>
<script>

    $(document).ready(function () {
        $('.owl-carousel').owlCarousel({
            navigation: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true,
                    loop: true
                },
                600: {
                    items: 3,
                    nav: false,
                    loop: true
                },
                1000: {
                    items: 5,
                    nav: true,
                    loop: true
                }
            },
            margin: 10,
            loop: 10,
            width: 20,
            nav: true,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true

        });
    });
</script>
<script src="<?= assets('theme1/js/scripts.js?r=210120') ?>"></script>
<script src="<?= assets('theme1/js/custom.js?r=210120') ?>"></script>

<?= $this->vars['site']['before_body'] ?>


</body>
</html>