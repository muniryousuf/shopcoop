<?php $cart = get_cart() ?>
<!DOCTYPE html>
<html>
<head>
    <title>Kensal</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style-front.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/queary.js">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.carousel.min.css">
    <script src="<?php echo base_url() ?>assets/js/owl.carousel.js"></script>
</head>
<body>

<header>

        <div class="row top-header">
            <div class="container">
                <div class="col-sm-12 text-center">
                    <a class="sign" href="<?php echo base_url('/my-account') ?>">Sign in</a>
                </div>
            </div>
        </div>
        <section class="middle-header mainmenu">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 text-center">
                    <a href="<?php echo base_url('/') ?>">
                        <img class="logo" src="<?php echo base_url() ?>assets/images/Kensal-logo.png"></a>
                </div>

                <div class="col-sm-3">
                    <div class="main-menu-nav">

                        <nav class="navbar navbar-expand-sm">

                            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarId">
                                 <i class="fas fa-bars mobile-menu"></i>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarId">

                                <ul class="navbar-nav mobile-menu-list">
                                    <li class="nav-item dropdown">
                                        <a href="" class="nav-link dropdown-toggle active-menuitem mobile-category-list" data-toggle="dropdown"
                                        data-target="dropdown_target">Categories
                                        </a>


                                        <ul class="dropdown-menu first-menu-list" aria-labeled-by="dropdown_target">
                                            <?php $categories = $this->ProCategories->getAllCategories();
                                            foreach ($categories as $key => $parent) { ?>
                                            <li class="nav-link dropdown"><a href="<?php echo url('/product-category/' . $parent['slug']) ?>" class="dropdown-item dropdown-toggle"><?php echo $parent['name'] ?></a>
                                                <ul class="dropdown-menu">
                                                    <?php  if(isset($parent['second_level']) &&  count($parent['second_level']) > 0) { foreach ($parent['second_level'] as $child) { ?>
                                                    <li>
                                                        <a href="<?php echo url('/product-category/' . $child['slug']) ?>" class="dropdown-item"><?php echo $child['name'] ?></a>
                                                    </li>
                                                    <?php  }} ?>
                                                </ul>
                                            </li>
                                            <?php } ?>

                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </nav>

                    </div>

                </div>
              <div class="col-sm-6">

              <div id="appendCartItems">

              </div>
            </div>
        </div>



</header>
    <section class="bottom-header">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-code">
                        <input autocomplete="seach-groceries" class="form-input" data-testid="seach-groceries"placeholder="Seach for groceries" type="text" value="">
                        <a href=""> <i class="fas fa-search"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>

<script>

(function($){
    $('.nav-link.dropdown').on('click', function(e) {
        if(!$(e.target).hasClass("dropdown-item")){
            $(this).find(".dropdown-menu").toggleClass("show");
          return false;
        }
    });

    $('.navbar-toggler').on('click', function(e) {
        $('.navbar-nav .nav-item.dropdown ul.dropdown-menu.first-menu-list').addClass("show")
    });



})(jQuery)

</script>
