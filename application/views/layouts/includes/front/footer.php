<footer class="footer">
    <div class="container">
        <div class="row">
           <!--  <div class="col-sm-12 footer-form-item">
                <p>Shop from a store near you</p>
                <div class="form-code">
                    <input autocomplete="postal-code" class="form-input" data-testid="postcode-input"placeholder="Enter a postcode" type="text" value="">
                    <a href=""> <i class="fas fa-map-marker-alt"></i></a>
                </div>
            </div> -->
            <div class="col-sm-12 links">
                <ul>
                    <li><a href="">Contact customer support</a></li>
                    <li><a href="">Find out about delivery and refunds</a></li>
                    <li><a href="">Terms of service</a></li>
                    <li><a href="">Contact customer support</a></li>
                    <li><a href="">Product recalls</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

    <div class="bottom_footer border-top-tran">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="mb-md-0 text-center text-md-left">© 2020 All Rights Reserved by Softechweb</p>
                </div>
                <div class="col-md-6">
                    <ul class="footer_payment text-center text-lg-right">
                        <li><a href="#"><img src="<?php echo base_url()?>assets/theme1/images/visa.png" alt="visa"></a></li>
                        <li><a href="#"><img src="<?php echo base_url()?>assets/theme1/images/discover.png" alt="discover"></a></li>
                        <li><a href="#"><img src="<?php echo base_url()?>assets/theme1/images/master_card.png" alt="master_card"></a></li>
                        <li><a href="#"><img src="<?php echo base_url()?>assets/theme1/images//paypal.png" alt="paypal"></a></li>
                        <li><a href="#"><img src="<?php echo base_url()?>assets/theme1/images/amarican_express.png" alt="amarican_express"></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>




<!-- Modal -->
<div class="modal fade featuredProductsModal"  tabindex="-1" role="dialog" aria-labelledby="featuredProductsModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Check Availability</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <!-- Modal Body -->
            <div class="modal-body availability-popup">
                <div class="form-group">
                    <?php $slots = json_decode($this->vars['site']['slots']); ?>
                    <label>Check Availability:</label>
                    <input type="text" class="form-control" placeholder="Enter a postcode" id="postalCode">
                    <i class="fas fa-map-marker-alt"></i>
                    <span id="postalMsg" style="color: red"></span>
                </div>
                <div class="form-group pt-0">
                    <label for="">Select Day</label>
                        <select class="custom-select form-control" id="select_day">
                          <option selected value="null">Select Day</option>
                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                          <option value="<?php  date('d/m/Y', strtotime('+'.$i.' day')) ?>"><?php echo date('d/m/Y', strtotime('+'.$i.' day')) ?></option>
                            <?php } ?>
                        </select>
                </div>
                <div class="form-group pt-0" id="select_time">
                    <label for="">Select Time</label>
                        <select class="custom-select form-control">
                          <option selected>Select Time</option>
                            <?php foreach ($slots as $s) {?>
                          <option value="<?php echo $s; ?>" id="<?php  echo $s;?>"> <?php echo $s; ?> </option>
                     <?php } ?>
                        </select>
                </div>
                <p>We need to know where you’re shopping from so that we can show the correct product range.</p>
            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="checkPostalCode">Save</button>
            </div>

        </div>
    </div>
</div>



<!---->
<!---->
<!--<footer class="footer_dark">-->
<!--    <div class="footer_top">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-lg-3 col-md-3 col-sm-6">-->
<!--                    <div class="widget">-->
<!--                        --><?php //$col2 = widget('footer-col2'); ?>
<!--                        <h6 class="widget_title">--><?//= strtoupper($col2['name']) ?><!--</h6>-->
<!--                        --><?//= $col2['content'] ?>
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-md-3 col-sm-6">-->
<!--                    <div class="widget">-->
<!--                        <h6 class="widget_title">Category</h6>-->
<!--                        --><?php //$categories = $this->ProCategories->getAllCategories(); ?>
<!--                        <ul class="widget_links">-->
<!--                            --><?php //foreach ($categories
//
//                            as $value) { ?>
<!--                            <a href="--><?php //echo url('/product-category/' . $value['slug']) ?><!--">--><?php //echo $value['name'] ?>
<!--                                --><?php //} ?>
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-md-6 col-sm-6">-->
<!--                    <div class="widget">-->
<!--                        <h6 class="widget_title">My Account</h6>-->
<!--                        <ul class="widget_links">-->
<!--                            --><?php //foreach (menu_items('footer-service') as $mi) { ?>
<!--                                <li><a class="nav-link" href="--><?//= $mi['link'] ?><!--">--><?//= $mi['label'] ?><!--</a></li>-->
<!--                            --><?php //} ?>
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--                <div class="col-lg-3 col-md-6 col-sm-12">-->
<!--                    <div class="widget">-->
<!--                        --><?php //$col1 = widget('footer-col1'); ?>
<!--                        <h6 class="widget_title">--><?//= strtoupper($col1['name']) ?><!--</h6>-->
<!--                        <p>--><?//= $col1['content'] ?><!--</p>-->
<!--                    </div>-->
<!--                    <div class="widget">-->
<!--                        <ul class="social_icons social_white">-->
<!--                            --><?php //$social_icons = $this->vars['site']['social_links'];
//                            print_r($social_icons); ?>
<!--                            --><?php //if (isset($social_icons['facebook'])) { ?>
<!--                                <li><a href="--><?php //echo $social_icons['facebook'] ?><!--" target="_blank"><i-->
<!--                                                class="ion-social-facebook"></i></a></li>-->
<!--                            --><?php //} ?>
<!--                            --><?php //if (isset($social_icons['instagram'])) { ?>
<!--                                <li><a href="--><?php //echo $social_icons['instagram'] ?><!--" target="_blank"><i-->
<!--                                                class="ion-social-instagram-outline"></i></a></li>-->
<!--                            --><?php //} ?>
<!---->
<!--                        </ul>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="bottom_footer border-top-tran">-->
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-md-6">-->
<!--                    <p class="mb-md-0 text-center text-md-left">© 2020 All Rights Reserved by Softechweb</p>-->
<!--                </div>-->
<!--                <div class="col-md-6">-->
<!--                    <ul class="footer_payment text-center text-lg-right">-->
<!--                        <li><a href="#"><img src="assets/theme1/images/visa.png" alt="visa"></a></li>-->
<!--                        <li><a href="#"><img src="assets/theme1/images/discover.png" alt="discover"></a></li>-->
<!--                        <li><a href="#"><img src="assets/theme1/images/master_card.png" alt="master_card"></a></li>-->
<!--                        <li><a href="#"><img src="assets/theme1/images//paypal.png" alt="paypal"></a></li>-->
<!--                        <li><a href="#"><img src="assets/theme1/images/amarican_express.png" alt="amarican_express"></a>-->
<!--                        </li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</footer>-->
<!---->

