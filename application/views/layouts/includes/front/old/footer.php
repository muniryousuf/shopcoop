
<footer class="footer_dark">
	<div class="footer_top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6">
                	<div class="widget">
                        <?php $col2 = widget('footer-col2'); ?>
                        <h6 class="widget_title"><?= strtoupper($col2['name']) ?></h6>
                        <?= $col2['content'] ?>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6">
                	<div class="widget">
                        <h6 class="widget_title">Category</h6>
                        <ul class="widget_links">
                            <li><a href="#">Men</a></li>
                            <li><a href="#">Woman</a></li>
                            <li><a href="#">Kids</a></li>
                            <li><a href="#">Best Saller</a></li>
                            <li><a href="#">New Arrivals</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6">
                	<div class="widget">
                        <h6 class="widget_title">My Account</h6>
                        <ul class="widget_links">
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Discount</a></li>
                            <li><a href="#">Returns</a></li>
                            <li><a href="#">Orders History</a></li>
                            <li><a href="#">Order Tracking</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12">
                    <div class="widget">
                        <?php $col1 = widget('footer-col1'); ?>
                        <h6 class="widget_title"><?= strtoupper($col1['name']) ?></h6>
                        <p><?= $col1['content'] ?></p>
                    </div>
                    <div class="widget">
                        <ul class="social_icons social_white">
                            <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                            <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                            <li><a href="#"><i class="ion-social-googleplus"></i></a></li>
                            <li><a href="#"><i class="ion-social-youtube-outline"></i></a></li>
                            <li><a href="#"><i class="ion-social-instagram-outline"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom_footer border-top-tran">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p class="mb-md-0 text-center text-md-left">© 2020 All Rights Reserved by Softechweb</p>
                </div>
                <div class="col-md-6">
                    <ul class="footer_payment text-center text-lg-right">
                        <li><a href="#"><img src="https://demo.softechweb.co.uk/assets/theme1/images/visa.png" alt="visa"></a></li>
                        <li><a href="#"><img src="https://demo.softechweb.co.uk/assets/theme1/images/discover.png" alt="discover"></a></li>
                        <li><a href="#"><img src="https://demo.softechweb.co.uk/assets/theme1/images/master_card.png" alt="master_card"></a></li>
                        <li><a href="#"><img src="https://demo.softechweb.co.uk/assets/theme1/images//paypal.png" alt="paypal"></a></li>
                        <li><a href="#"><img src="https://demo.softechweb.co.uk/assets/theme1/images/amarican_express.png" alt="amarican_express"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>