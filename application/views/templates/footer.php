
<footer class="footer">
    <div class="container">
        <div class="row">
       <!--      <div class="col-sm-12 footer-form-item">
                <p>Shop from a store near you</p>
                <div class="form-code">
                    <input autocomplete="postal-code" class="form-input" data-testid="postcode-input"placeholder="Enter a postcode" type="text" value="">
                    <a href=""> <i class="fas fa-map-marker-alt"></i></a>
                </div>
            </div> -->
            <div class="col-sm-12 links">
                <ul>
                    <li><a href="">Contact customer support</a></li>
                    <li><a href="">Find out about delivery and refunds</a></li>
                    <li><a href="">Terms of service</a></li>
                    <li><a href="">Contact customer support</a></li>
                    <li><a href="">Product recalls</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>


<script>

    $(document).ready(function () {
        $('.owl-carousel').owlCarousel({
            navigation: true,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true,
                    loop: true
                },
                600: {
                    items: 3,
                    nav: false,
                    loop: true
                },
                1000: {
                    items: 6,
                    nav: true,
                    loop: true
                }
            },
            margin: 10,
            loop: 10,
            width: 20,
            nav: true,
            navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true

        });
    });
</script>

<!-- Option 1: Bootstrap Bundle with Popper -->

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
        crossorigin="anonymous"></script>


<!-- Option 2: Separate Popper and Bootstrap JS -->

</body>
</html>
