<?php $cart = get_cart() ?>

<!DOCTYPE html>
<html>
<head>
    <title>REDBRIDGE</title>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style-front.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/js/queary.js">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/owl.carousel.min.css">
    <script src="<?php echo base_url() ?>assets/js/owl.carousel.js"></script>
</head>
<body>


<header>

    <div class="container-fluid">

        <div class="row top-header">
            <div class="col-sm-12">
                <p>WE CAN HELP - <a href="#">0000 0000 000</a></p>
            </div>
        </div>

        <div class="row middle-header">
            <div class="col-sm-3">
                <a href="#"><img class="logo" src="<?php echo base_url() ?>assets/images/lredbridge-logo.png"></a>
            </div>
            <div class="col-sm-6">
                <form class="serach" action="">
                    <input type="text" placeholder="Search entire store here..">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>

            <div style="display: inherit;" class="col-sm-3 text-center">
                <a href="#"><i class="fa fa-user" aria-hidden="true"></i><br>My Account</a>
                <a style="padding-left: 35px;" href="#"><i class="fa fa-shopping-cart" aria-hidden="true"></i><br>
                    Basket</a>
            </div>
        </div>

        <div class="row bottom-header">
            <div class="col-sm-12 menu">
                <ul class="nav navbar-nav">
                    <li><a href="#">Home</a></li>
                    <li><a href="#">Product</a></li>
                    <li><a href="#">Product</a></li>
                    <li><a href="#">Product</a></li>
                    <li><a href="#">Product</a></li>
                    <li><a href="#">Product</a></li>
                    <li><a href="#">Product</a></li>
                    <li><a href="#">Product</a></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Simulate a smartphone / tablet -->
    <div class="mobile-container">
        <!-- Top Navigation Menu -->
        <div class="topnav">
            <a href="#home" class="active"></a>
            <div id="myLinks">
                <a href="#">Home</a>
                <a href="#">Product</a>
                <a href="#">Product</a>
                <a href="#">Product</a>
                <a href="#">Product</a>
                <a href="#">Product</a>
                <a href="#">Product</a>
                <a href="#">Product</a>
                <a href="#">Product</a>
                <a href="#"></a>
            </div>
            <a href="javascript:void(0);" class="icon" onclick="myFunction()">
                <i class="fa fa-bars"></i>
            </a>
        </div>
    </div>
    <!-- Simulate a smartphone / tablet -->

</header>
