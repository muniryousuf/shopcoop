<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Order Invoice</title>
<style type="text/css">
<?= file_get_contents(assets('css/mpdf.css')) ?>
</style>
</head>
<body>
	<table class="head container">
		<tr>
			<td class="header">
				<?php
				if($this->vars['site']['header_logo']) {
					echo '<img src="'. uploaded_url($this->vars['site']['header_logo']) .'" />';
				} else {
					echo $this->vars['site']['site_name'];
				}
				?>
			</td>
			<td class="shop-info">
				<div class="shop-name"><h3><?= $this->vars['site']['site_name'] ?></h3></div>
				<div class="shop-address">
					Retouradres:
					<br />
					<br />
					<?= nl2br($this->vars['return_address']) ?>
				</div>
			</td>
		</tr>
	</table>

	<h1 class="document-type-label">INVOICE</h1>

	<table class="order-data-addresses">
		<tr>
			<td class="address billing-address">
				<?php
                $addr = []; 
                foreach ($address as $a) {
                    if($a['type'] == 1) {
                        $addr = $a;
                    }
                }

                if($addr) {
                    echo $addr['first_name'] . ' ' . $addr['last_name'];
                    echo '<br />';
                    echo $addr['address1'];
                    echo '<br />';
                    echo $addr['address2'];
                    echo '<br />';
                    echo $addr['city'] . ', ' . $addr['postalcode'];
                    echo '<br />';
                    echo $addr['country'];
                    echo '<br />';
                    echo '<br />';
                }
                ?>
				<div class="billing-email"><?= $order['email'] ?></div>
				<div class="billing-phone"><?= $order['phone'] ?></div>
			</td>
			<td class="address shipping-address">
				<?php
                $addr = []; 
                foreach ($address as $a) {
                    if($a['type'] == 2) {
                        $addr = $a;
                    }
                }

                if($addr) {
                	echo '<h3>Ship To:</h3>';
                    echo $addr['first_name'] . ' ' . $addr['last_name'];
                    echo '<br />';
                    echo $addr['address1'];
                    echo '<br />';
                    echo $addr['address2'];
                    echo '<br />';
                    echo $addr['city'] . ', ' . $addr['postalcode'];
                    echo '<br />';
                    echo $addr['country'];
                }
                ?>
			</td>
			<td class="order-data">
				<table>
					<tr class="invoice-number">
						<th>Factuurnummer:</th>
						<td><?= $invoice_no; ?></td>
					</tr>
					<tr class="invoice-date">
						<th>Invoice date:</th>
						<td><?= date('d-M-Y', strtotime($order['created_at'])) ?></td>
					</tr>
					<tr class="order-number">
						<th>Ordernummer:</th>
						<td><?= $order['id'] ?></td>
					</tr>
					<tr class="order-date">
						<th>Datum order:</th>
						<td><?= date('d-M-Y', strtotime($order['created_at'])) ?></td>
					</tr>
					<tr class="payment-method">
						<th>Betaalmethode:</th>
						<td><?= $order['payment_method'] ?></td>
					</tr>
				</table>			
			</td>
		</tr>
	</table>

	<table class="order-details">
		<thead>
			<tr>
				<th class="product">Product</th>
				<th class="quantity">Aantal</th>
				<th class="price">Prijs</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($products as $p) { ?>
            <tr>
                <td class="product"><?= $p['name'] ?></td>
                <td class="quantity"><?= $p['quantity'] ?></td>
                <td class="price"><?= format_money($p['price']) ?></td>
            </tr>
            <?php } ?>
		</tbody>

		<tfoot>
			<tr class="no-borders">
				<td class="no-borders">
					<div class="customer-notes">
						<?php if($order['comments']) { ?>
						<h3>Customer Notes</h3>
						<?= $order['comments'] ?>
						<?php } ?>
					</div>				
				</td>
				<td class="no-borders" colspan="2">&nbsp;</td>
			</tr>
		</tfoot>
	</table>

	<table class="totals">
		<tr>
			<td class="no-borders"></td>
			<th class="description">Subtotaal</th>
			<td class="price"><span class="totals-price"><?= format_money($order['sub_total']) ?></span></td>
		</tr>
		<tr>
			<td class="no-borders"></td>
			<th class="description">Verzending</th>
			<td class="price">Gratis verzending</td>
		</tr>
		<tr class="order_total">
			<td class="no-borders"></td>
			<th class="description">Totaal</th>
			<td class="price">
				<strong><?= format_money($order['net_total']) ?>
				<?php if($order['sub_total'] == $order['net_total'] && $order['tax']) { ?>
				<small class="includes_tax">(inclusief <?= format_money($order['tax']) ?> <?= round($order['tax']/$order['sub_total']*100, 2) ?>% BTW)</small>
				<?php } ?>
				</strong>
			</td>
		</tr>
	</table>
	<div id="footer">Retouren binnen 14 dagen.</div>
</body>
</html>