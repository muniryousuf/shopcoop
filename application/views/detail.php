<section class="shop-sec-1 container-fluid">
    <div class="row">
        <div class="col-sm-5">
            <img src="<?php echo base_url() ?>assets/images/ab000500_1_1.jpg">
        </div>
        <div style="" class="col-sm-7 shade">
            <div class="box-shade">
                <h2 style="color: #565656;">Forterra LBC Honey Buff Pressed Facing Brick Pack of 390</h2>
                <p class="sku">SKU <span>AB000500</span></p>
                <h3 class="price">£433.00 <span>Excl. Vat</span></h3>
                <p class="price">£433.00 <span>Excl. Vat</span></p>


                <div class="addtocart-selector small">
                    <label>QTY</label>
                    <div class="addtocart-qty">
                        <div class="addtocart-button button-down"><span class="glyphicon glyphicon-minus"
                                                                        aria-label="increase quantity"></span></div>
                        <input type="text" class="addtocart-input" value="1"/>
                        <div class="addtocart-button button-up">
                            <span class="glyphicon glyphicon-plus" aria-label="increase quantity"></span>
                        </div>
                    </div>
                    <button>ADD TO CART</button>
                </div>


                <h4 style="font-size: 20px; font-weight: 700;">✅ Expected Lead Time for Delivery / Click & Collect : 3-5
                    Working Days</h4>
            </div>
        </div>
    </div>
    <br>
</section>

<section style="padding: 20px;" class="shop-sec-2">
    <div class="row">
        <div class="col-sm-1 color custom-tab">
            <div class="review">
                <img class="review" src="<?php echo base_url() ?>assets/images/reviews.png">
                <p>Reviews</p>
            </div>
        </div>
        <div class="col-sm-12">
            <div class="review-text">
                <p>You're reviewing:</p>
                <h4>Forterra LBC Honey Buff Pressed Facing Brick<br> Pack of 390</h4><br>

                <div class="col-lg-12">
                    <div class="star-rating">
                        <span class="Rating">Your Rating</span>
                        <span class="fa fa-star-o" data-rating="1"></span>
                        <span class="fa fa-star-o" data-rating="2"></span>
                        <span class="fa fa-star-o" data-rating="3"></span>
                        <span class="fa fa-star-o" data-rating="4"></span>
                        <span class="fa fa-star-o" data-rating="5"></span>
                        <input type="hidden" name="whatever1" class="rating-value" value="2.56">
                    </div>
                </div>
                <div class="col-sm-6">
                    <form>
                        <div class="form-group">
                            <div class="col-sm-2">
                                <span>Nickname</span>
                            </div>
                            <input type="mail" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                   placeholder="">
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2">
                                <span>Summary</span>
                            </div>
                            <input type="mail" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                   placeholder="">
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2">
                                <span>Review</span>
                            </div>
                            <input type="mail" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                                   placeholder="">
                        </div>

                        <div class="form-group">
                            <div class="col-sm-2">
                                <span></span>
                            </div>
                            <a href="">
                                <button>Submit Review</button>
                            </a>
                        </div>


                    </form>
                </div>

            </div>
        </div>
</section>