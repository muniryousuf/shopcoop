<p>Hallo <?= $first_name ?>,</p>
<p>Iemand heeft een nieuw wachtwoord aangevraagd voor het volgende account op <?= site_title() ?>:</p>
<p>E-mailadres: <?= $email ?>
<p>Als je dit niet hebt aangevraagd, kun je deze e-mail negeren. Als je wilt doorgaan:</p>
<p><a href="<?= $reset_url ?>">Klik hier om je wachtwoord opnieuw in te stellen</a></p>
<p>Bedankt voor het lezen.</p>