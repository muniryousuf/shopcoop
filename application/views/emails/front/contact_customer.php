<p>Hallo <?= $name ?>,</p>
<p>We hebben je bericht ontvangen en zullen binnen 2 werkdagen een reactie geven. Hieronder vind je een kopie van je bericht.</p>
<p><strong>Naam:</strong> <?= $name ?></p>
<p><strong>E-mail:</strong> <?= $email ?></p>
<p><strong>Telefoon:</strong> <?= $phone ?></p>
<p><strong>Bericht:</strong> <?= nl2br($message) ?></p>