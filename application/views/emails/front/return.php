<p>Hallo</p>
<p>Er is een nieuw bericht verstuurd via <?= $name ?>. Hieronder de inhoud van het bericht:</p>

<p><strong>Retourformulier</strong></p>
<p><strong>Ordernummer:</strong> <?= $orderno ?></p>
<p><strong>Voor- en achternaam:</strong> <?= $name ?></p>
<p><strong>E-mailadres:</strong> <?= $email ?></p>
<p><strong>Telefoon:</strong> <?= $phone ?></p>
<p><strong>Leverdatum:</strong> <?= $delivery_date ?></p>

<p><strong>Reden retour:</strong> <?= $reason1 ?></p>
<p><strong>Artikel:</strong> <?= $item1 ?></p>
<p><strong>Maat:</strong> <?= $measure1 ?></p>

<p><strong>Reden retour:</strong> <?= $reason2 ?></p>
<p><strong>Artikel:</strong> <?= $item2 ?></p>
<p><strong>Maat:</strong> <?= $measure2 ?></p>

<p><strong>Reden retour:</strong> <?= $reason3 ?></p>
<p><strong>Artikel:</strong> <?= $item3 ?></p>
<p><strong>Maat:</strong> <?= $measure3 ?></p>

<p><strong>Opmerkingen:</strong> <?= nl2br($comments) ?></p>