<p>Hallo <?= $first_name ?>,</p>
<p>Bedankt voor het aanmaken van een account bij <?= site_title() ?>.</p>
<p>Je kunt je accountgebied openen om bestellingen te bekijken, je wachtwoord te wijzigen en meer, via: <a href="<?= account_url() ?>"><?= account_url() ?></a></p>
<p>We kijken ernaar uit om je te zien.</p>