<?php
$this->js = [
    assets('js/jquery.validate.min.js'),
    assets('js/additional-methods.min.js'),
];
?>
<form id="frm-contact" action="" method="post">
    <div class="row">
		<div class="col-md-6">
			<div class="err"></div>

			<div class="form-group">
				<label for="name">Name <span class="required">*</span></label>
				<input type="text" id="name" name="name" class="form-control" value="" required />
			</div>

			<div class="form-group">
				<label for="email">E-mail <span class="required">*</span></label>
				<input type="email" id="email" name="email" class="form-control" value="" required />
			</div>

			<div class="form-group">
				<label for="phone">Telephone</label>
				<input type="text" id="phone" name="phone" class="form-control" value="" />
			</div>

			<div class="form-group">
				<label for="message">Message <span class="required">*</span></label>
				<textarea id="message" name="message" class="form-control" required></textarea>
			</div>

			<button type="submit" class="theme_btn text-uppercase text-center">Bericht versturen</button>
		</div>
	</div>
</form>
