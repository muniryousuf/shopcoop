<?php
$products = $this->ProCategories->getAllFeaturesProducts();
$cart   = $this->session->userdata('cart');
$cartProducts = array();
if($cart && count($cart) > 0){
    foreach ($cart as $k => $v){
        $cartProducts[$v['product']] = $v['quantity'];
    }
}
?>

<section class="home-sec-3 container-fluid">
    <div class="row">
        <div class="col-sm-12 text-center">
            <h2>BEST<span> SELLER</span> PROD<span>UCTS</span></h2>
        </div>
    </div>

    <!--carousel fedi-->
    <div class="container featured-product-section">
        <div class="row">
            <div class="col-md-12">
                <div class="owl-carousel owl-loaded owl-drag">
                    <div class="owl-stage-outer">
                        <div class="owl-stage"
                             style="transform: translate3d(-4246px, 0px, 0px); transition: all 0.25s ease 0s; width: 100%;">

                            <?php foreach ($products as $product)  {?>
                                <div class="owl-item active"  >
                                    <div class="item product-box">
                                        <a href="<?= url('/product/' . $product['slug']) ?>">
                                            <img src="<?php echo $product['image'] ?>"
                                                 class="d-block w-100 mb-2" alt=""></a>
                                        <div class="featured-detail">
                                            <p class="truncate"><?php  echo $product['name']?></p>
                                            <p class="text-before-2">£ <?php  echo $product['price']?></p>
                                            <a href="javascript:;">
                                                <!-- <button>Details</button> -->
                                                <button type="button" href="<?= url('/product/' . $product['slug']) ?>" class="btn btn-primary openPostalCode">Details</button>
                                                <input type="hidden" value="<?= url('/product/' . $product['slug']) ?>">
                                            </a>
                                        </div>
                                        <div class="number">
                                                      <span class="minus" id="<?php echo  $product['id'] ?>">-

                                                      </span>

                                            <input type="text" value="<?php echo isset($cartProducts[$product['id']])?$cartProducts[$product['id']]:0;  ?>"/>
                                            <span class="plus" id="<?php echo  $product['id'] ?>">+
                                                      </span>
                                        </div>
                                    </div>
                                </div>
                            <?php  } ?>

                            <!--                <div class="owl-item active ">-->
                            <!--                    <div class="item product-box">-->
                            <!--                        <a href=""><img src="--><?php //echo base_url() ?><!--assets/images/ab000560_1_1.jpg"-->
                            <!--                                        class="d-block w-100" alt=""></a>-->
                            <!--                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>-->
                            <!--                        <p class="text-before">£0.30<span>Excl Vat</span></p>-->
                            <!--                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>-->
                            <!--                        <a style="padding-left: 25px;" href="">-->
                            <!--                            <button>ADD TO CART</button>-->
                            <!--                        </a>-->
                            <!--                    </div>-->
                            <!--                </div>-->
                            <!--                <div class="owl-item active ">-->
                            <!--                    <div class="item product-box">-->
                            <!--                        <a href=""><img src="--><?php //echo base_url() ?><!--assets/images/ab000560_1_1.jpg"-->
                            <!--                                        class="d-block w-100" alt=""></a>-->
                            <!--                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>-->
                            <!--                        <p class="text-before">£0.30<span>Excl Vat</span></p>-->
                            <!--                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>-->
                            <!--                        <a style="padding-left: 25px;" href="">-->
                            <!--                            <button>ADD TO CART</button>-->
                            <!--                        </a>-->
                            <!--                    </div>-->
                            <!--                </div>-->
                            <!--                <div class="owl-item active ">-->
                            <!--                    <div class="item product-box">-->
                            <!--                        <a href=""><img src="--><?php //echo base_url() ?><!--assets/images/ab000560_1_1.jpg"-->
                            <!--                                        class="d-block w-100" alt=""></a>-->
                            <!--                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>-->
                            <!--                        <p class="text-before">£0.30<span>Excl Vat</span></p>-->
                            <!--                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>-->
                            <!--                        <a style="padding-left: 25px;" href="">-->
                            <!--                            <button>ADD TO CART</button>-->
                            <!--                        </a>-->
                            <!--                    </div>-->
                            <!--                </div>-->
                            <!--                <div class="owl-item active ">-->
                            <!--                    <div class="item product-box">-->
                            <!--                        <a href=""><img src="--><?php //echo base_url() ?><!--assets/images/ab000560_1_1.jpg"-->
                            <!--                                        class="d-block w-100" alt=""></a>-->
                            <!--                        <p>Larchfield Paving Block 50mm Brindle 200 x 100 (Burnt Ember)</p>-->
                            <!--                        <p class="text-before">£0.30<span>Excl Vat</span></p>-->
                            <!--                        <p class="text-before-2">£0.30<span>Excl Vat</span></p>-->
                            <!--                        <a style="padding-left: 25px;" href="">-->
                            <!--                            <button>ADD TO CART</button>-->
                            <!--                        </a>-->
                            <!--                    </div>-->
                            <!--                </div>-->


                        </div>
                    </div>
                    <div class="owl-nav disabled">
                        <button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button>
                        <button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button>
                    </div>
                    <div class="owl-dots">
                        <button role="button" class="owl-dot"><span></span></button>
                        <button role="button" class="owl-dot"><span></span></button>
                        <button role="button" class="owl-dot active"><span></span></button>
                        <button role="button" class="owl-dot"><span></span></button>
                    </div>
                </div>
            </div>
        </div>
    </div>


</section>

