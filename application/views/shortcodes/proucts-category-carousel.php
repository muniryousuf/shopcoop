<?php
$id = explode(',', $name);

$categories = $this->ProCategories->getCategoryProduct($id[0]);
?>
<?php foreach ($categories as $k => $v) { ?>
    <?php if(count($categories[$k])>0) {?>
    <section class="grid-box-sec">
        <div class="container">
            <div class="title">
                <h3 class="">#<?php echo $k; ?></h3>
            </div>
            <div class="box-wrap">
                <ul class="row">
                    <?php foreach ($categories[$k] as $value) { ?>
                        <li class="col-xs-12 col-sm-4">
                            <div class="box">
                                <div class="box-image">
                                    <a href="<?= url('/product/' . $value['slug']) ?>">
                                        <img src="<?php echo $value['image'] ?>" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="des">
                                <h4 style="font-size: 16px;color: #000;">
                                    <span class=""><?php echo $value['name'] ?></span>
                                </h4>
                                <p style="font-size: 12px;color: #2d2d2d;">
                                <span class="">

                                      <?php if ($value['sale_price']) { ?>
                                          <del class="text_gray"><?= format_money($value['price']) ?></del>
                                          <span class="text_black"><?= format_money($value['sale_price']) ?></span>
                                      <?php } else { ?>
                                          <span class="text_black"><?= format_money($value['price']) ?></span>
                                      <?php } ?>
                                </p>
                                <a class="btn custom-btn2" href="<?= url('/product/' . $value['slug']) ?>">
                                    <span class="">Shop Now</span>
                                </a>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
            </div>

        </div>
    </section>

<?php } ?>
<?php } ?>



