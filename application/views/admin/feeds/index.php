<?= form_open(admin_url('feeds/store')) ?>

<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">Google</a></li>
        <li><a href="#tab_2" data-toggle="tab">Facebook</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <div class="form-group">
                <label>Feed URL:</label>
                <input type="text" value="<?= url('/google_feed.xml') ?>" class="form-control" readonly />
                <a href="<?= url('/google_feed.xml') ?>" target="_blank"> Download</a>
            </div>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="2">Google Analytics Settings</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="40%">Google Analytics campaign source (utm_source):</td>
                        <td><input type="text" name="vars[google_utm_source]" value="<?= $vars_val['google_utm_source'] ?>" class="form-control" /></td>
                    </tr>
                    <tr>
                        <td>Google Analytics campaign medium (utm_medium):</td>
                        <td><input type="text" name="vars[google_utm_medium]" value="<?= $vars_val['google_utm_medium'] ?>" class="form-control" /></td>
                    </tr>
                    <tr>
                        <td>Google Analytics campaign name (utm_campaign):</td>
                        <td><input type="text" name="vars[google_utm_campaign]" value="<?= $vars_val['google_utm_campaign'] ?>" class="form-control" /></td>
                    </tr>
                    <tr>
                        <td>Google Analytics campaign term (utm_term):</td>
                        <td><input type="text" value="(dynamically added Product ID)" class="form-control" readonly /></td>
                    </tr>
                    <tr>
                        <td>Google Analytics campaign content (utm_content):</td>
                        <td><input type="text" name="vars[google_utm_content]" value="<?= $vars_val['google_utm_content'] ?>" class="form-control" /></td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div class="tab-pane" id="tab_2">
            <div class="form-group">
                <label>Feed URL:</label>
                <input type="text" value="<?= url('/facebook_feed.xml') ?>" class="form-control" readonly />
                <a href="<?= url('/facebook_feed.xml') ?>" target="_blank"> Download</a>
            </div>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="2">Google Analytics Settings</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="40%">Google Analytics campaign source (utm_source):</td>
                        <td><input type="text" name="vars[fb_utm_source]" value="<?= $vars_val['fb_utm_source'] ?>" class="form-control" /></td>
                    </tr>
                    <tr>
                        <td>Google Analytics campaign medium (utm_medium):</td>
                        <td><input type="text" name="vars[fb_utm_medium]" value="<?= $vars_val['fb_utm_medium'] ?>" class="form-control" /></td>
                    </tr>
                    <tr>
                        <td>Google Analytics campaign name (utm_campaign):</td>
                        <td><input type="text" name="vars[fb_utm_campaign]" value="<?= $vars_val['fb_utm_campaign'] ?>" class="form-control" /></td>
                    </tr>
                    <tr>
                        <td>Google Analytics campaign term (utm_term):</td>
                        <td><input type="text" value="(dynamically added Product ID)" class="form-control" readonly /></td>
                    </tr>
                    <tr>
                        <td>Google Analytics campaign content (utm_content):</td>
                        <td><input type="text" name="vars[fb_utm_content]" value="<?= $vars_val['fb_utm_content'] ?>" class="form-control" /></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="form-group">
    <button type="submit" class="btn btn-primary">Save</button>
</div>

<?= form_close() ?>
