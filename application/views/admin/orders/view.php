<section class="invoice" style="margin: 0;">
    <!-- title row -->
    <div class="row">
        <div class="col-xs-12">
            <h2 class="page-header">
                <i class="fa fa-globe"></i> Order #<?= $order['id'] ?>
                <small class="pull-right">Date: <?= date('d/M/Y h:i a', strtotime($order['created_at'])) ?></small>
            </h2>
        </div>
        <!-- /.col -->
    </div>
    <!-- info row -->
    <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
            <address>
                <strong>Billing Address</strong><br>
                <?php
                $addr = []; 
                foreach ($address as $a) {
                    if($a['type'] == 1) {
                        $addr = $a;
                    }
                }

                if($addr) {
                    echo $addr['first_name'] . ' ' . $addr['last_name'];
                    echo '<br />';
                    echo $addr['address1'];
                    echo '<br />';
                    echo $addr['address2'];
                    echo '<br />';
                    echo $addr['city'] . ', ' . $addr['postalcode'];
                    echo '<br />';
                    echo $addr['country'];
                }
                ?>
                <br><br />
                Phone: <?= $order['phone'] ?><br>
                Email: <a href="mailto:<?= $order['email'] ?>"><?= $order['email'] ?></a>
            </address>
        </div>

        <div class="col-sm-4 invoice-col">
            <address>
                <strong>Shipping Address</strong><br>
                <?php
                $addr = []; 
                foreach ($address as $a) {
                    if($a['type'] == 2) {
                        $addr = $a;
                    }
                }

                if($addr) {
                    echo $addr['first_name'] . ' ' . $addr['last_name'];
                    echo '<br />';
                    echo $addr['address1'];
                    echo '<br />';
                    echo $addr['address2'];
                    echo '<br />';
                    echo $addr['city'] . ', ' . $addr['postalcode'];
                    echo '<br />';
                    echo $addr['country'];
                } else {
                    echo 'Same as billing address';
                }
                ?>
            </address>
        </div>

        <div class="col-sm-4 invoice-col">
            <b>Payment Method:</b> <?= $order['payment_method'] ?><br />

            <form action="" method="POST">
                <div class="form-group">
                    <label>Status:</label>
                    <select name="status" class="form-control">
                        <?php foreach (order_statuses() as $k => $v) { ?>
                        <option value="<?= $k ?>" <?= $k == $order['status'] ? 'selected' : '' ?>><?= $v ?></option>
                        <?php } ?>
                    </select>
                </div>
                <button type="submit" name="submit" class="btn btn-primary">Update Status</button>
            </form>

            <hr />

<!--            <a href="--><?//= admin_url('orders/invoice/' . $order['id']) ?><!--" class="btn btn-default" target="_blank" style="margin-right: 20px;">-->
<!--                PDF Invoice --><?//= $order['invoice_file'] ? '<i class="fa fa-check text-green"></i>' : '' ?><!-- -->
<!--            </a>-->
<!---->
<!--            <a href="--><?//= admin_url('orders/packingslip/' . $order['id']) ?><!--" class="btn btn-default" target="_blank">-->
<!--                PDF Packing Slip --><?//= $order['packing_slip_file'] ? '<i class="fa fa-check text-green"></i>' : '' ?>
<!--            </a>-->
        </div>
    </div>

    <!-- Table row -->
    <div class="row">
        <div class="col-xs-12 table-responsive">
            <p class="lead">Products:</p>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Qty</th>
                        <th style="text-align: right;">Price</th>
                        <th style="text-align: right;">Subtotal</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($products as $p) { ?>
                    <tr>
                        <td><?= $p['name'] ?></td>
                        <td><?= $p['quantity'] ?></td>
                        <td align="right"><?= format_money($p['price']) ?></td>
                        <td align="right"><?= format_money($p['price'] * $p['quantity']) ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-xs-4 pull-right">
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td align="right"><?= format_money($order['sub_total']) ?></td>
                    </tr>
                    <tr>
                        <th>Shipping</th>
                        <td align="right">Gratis verzending</td>
                    </tr>
                    <tr>
                        <th>Total:</th>
                        <td align="right">
                            <strong><?= format_money($order['net_total']) ?>
                            <?php if($order['sub_total'] == $order['net_total'] && $order['tax']) { ?>
                            <small class="includes_tax">(inclusief <?= format_money($order['tax']) ?> <?= round($order['tax']/$order['sub_total']*100, 2) ?>% BTW)</small>
                            <?php } ?>
                            </strong>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</section>

<div class="row" style="margin-top: 20px;">
    <div class="col-sm-6">
        <a href="<?= admin_url('orders') ?>" class="btn btn-default">Back To Orders</a>
    </div>
    <div class="col-sm-6 text-right">
        <?php if($prev_id) { ?>
        <a href="<?= admin_url('orders/view/' . $prev_id) ?>" class="btn btn-default">Previous Order</a>
        <?php } ?>
        <?php if($next_id) { ?>
        <a href="<?= admin_url('orders/view/' . $next_id) ?>" class="btn btn-default">Next Order</a>
        <?php } ?>
    </div>
</div>