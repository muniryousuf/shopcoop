<div class="box box-primary">
    <div class="box-body">
        <?= form_open() ?>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="title" class="required">Title</label>
                    <input type="text" id="title" name="name" class="form-control" value="<?= isset($row['name']) ? $row['name'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="title" class="required">Slug</label>
                    <input type="text" id="slug" name="slug" class="form-control" value="<?= isset($row['slug']) ? $row['slug'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="status" class="required">Status</label>
                    <select id="status" name="status" class="form-control">
                        <option value="1" <?= isset($row['status']) && $row['status'] == 1 ? 'selected' : '' ?>>Published</option>
                        <option value="0" <?= isset($row['status']) && $row['status'] == 0 ? 'selected' : '' ?>>Draft</option>
                    </select>
                </div>

               <div class="form-group col-sm-12">
                <h4>Add Images</h4>
                <div id="other_images" class="row">
                    <?php if(isset($row['images'])) {  ?>
                        <?php foreach ($row['images'] as $k => $img) { ?>
                            <div class="rr">
                            <div class="form-group col-sm-4 img-form-group">
                                <div class="input-group">
                            <span class="input-group-btn">
                                <a href="<?= file_manager('image' . $k) ?>" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>
                            </span>
                                    <input type="text" id="oimage<?= $k ?>" name="images[<?= $k ?>][image]" value="<?= $img['image'] ?>" class="form-control" readonly />
                                    <span class="input-group-btn">
                                <a href="javascript:void(0)" class="btn btn-danger remove-img"><i class="fa fa-timex"></i></a>
                            </span>
                                </div>
                                <div style="background: #ccc; display: inline-block; padding: 5px; margin-top: 10px;">
                                    <img class="img-responsive" id="oimage<?= $k ?>-preview" style="max-height: 100px;" src="<?= $img['image'] ? uploaded_url($img['image']) : '' ?>" />
                                </div>
                                <input type="hidden" name="images[<?= $k ?>][id]" value="<?= $img['id'] ?>" />
                            </div>

                                <div class="form-group col-sm-3">
                                    <input type="url" id="slug" class="form-control" name="images[<?= $k ?>][slider_url]"
                                           value="<?= $img['slider_url'] ?>" placeholder="Sort"/>
                                </div>
                                <div class="form-group col-sm-2">
                                    <input  class="form-control" type="text" id="slug" name="images[<?= $k ?>][alt_txt]"
                                           value="<?= $img['alt_txt'] ?>" placeholder="Alt Text"/>
                                </div>

                                <div class="form-group col-sm-2">
                                    <input class="form-control" type="text" id="slug" name="images[<?= $k ?>][sort_order]"
                                           value="<?= $img['sort'] ?>" placeholder="Sort"/>
                                </div>
                            </div>

                        <?php } ?>
                    <?php } ?>
                </div>

                <a href="javascript:void(0)" class="btn btn-primary another-img">+ Add Image</a>
                </div>



                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button> 
                    <a href="<?= admin_url('slider') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>

<script type="application/javascript">
    var imgcnt = <?= isset($row['images']) ? count($row['images']) : 0 ?>;
$(function(){
    if($("#slug").val() == '') {
        $("#title").on('input',function(e){
            $("#slug").val(slugify($(this).val()));
        });
    }

    $('.another-img').click(function(){
        var img  = '<div class="rr">';
        img +=  '<div class="form-group col-sm-4 img-form-group">';
        img += '  <div class="input-group">';
        img += '    <span class="input-group-btn">';
        img += '      <a href="<?= file_manager('oimage') ?>' + imgcnt + '" class="btn btn-primary upload-btn"><i class="fa fa-picture-o"></i> Choose</a>';
        img += '    </span>';
        img += '    <input type="text" id="oimage' + imgcnt + '" name="images[' + imgcnt + '][image]" class="form-control image" readonly />';
        img += '    <span class="input-group-btn">';
        img += '      <a href="javascript:void(0)" class="btn btn-danger remove-img"><i class="fa fa-times"></i></a>';
        img += '    </span>';
        img += '  </div>';
        img += '  <div style="background: #ccc; display: inline-block; padding: 5px; margin-top: 10px;">';
        img += '    <img class="img-responsive" id="oimage' + imgcnt + '-preview" style="max-height: 100px;" src="" />';
        img += '  </div>';
        img += '  <input type="hidden" name="images[' + imgcnt + '][id]" value="" />';
        img += '</div>';
        img += '<div class="form-group col-sm-3">';
        img +=  '<input type="url" id="slot"  name="images[' + imgcnt + '][slider_url]" value=""  class="form-control" placeholder="Redirect Url"/>';
        img +=  '</div>';

        img += '<div class="form-group col-sm-2">';
        img +=  '<input type="text" id="images[' + imgcnt + ']alt_text"  name="images[' + imgcnt + '][alt_txt]" value=""  class="form-control"  placeholder="Alt Text" "/>';
        img +=  '</div>';

        img += '<div class="form-group col-sm-2">';
        img +=  '<input type="number" id="slot"  name="images[' + imgcnt + '][sort_order]" value=""  class="form-control" placeholder="Sort Order"/>';
        img +=  '</div>';
        img +=  '</div>';


        $('#other_images').append(img);
        imgcnt++;

        $('#other_images .upload-btn').fancybox({
            width: 900,
            minHeight: 600,
            type: 'iframe',
            autoScale: true
        });
    });

    $('.clear-img').click(function(){
        if($('#image').val() != '' ) {
            if(confirm('Are you sure you want to remove this image?')) {
                $('#image').val('');
                $('#image-preview').attr('src', '');
            }
        }
    });

    $(document).on('click', '.remove-img', function() {
        if(confirm('Are you sure you want to remove this image?')) {
            $(this).parents('.rr').remove();
            $(this).parents('.img-form-group').remove();
        }
    });
})
</script>