<div class="row">
	<div class="col-md-6">
		<h3 style="margin: 0;">Welcome <?= $this->user['first_name'] . ' ' . $this->user['last_name'] ?></h3>
	</div>
	<div class="col-md-6 text-right">
		<button type="button" class="btn btn-default pull-right" id="daterange-btn">
            <span><i class="fa fa-calendar"></i> <?= date('F d, Y', strtotime($start_date)) ?> - <?= date('F d, Y', strtotime($end_date)) ?></span>
            <i class="fa fa-caret-down"></i>
        </button>
	</div>
</div>

<hr style="border-color: #ddd;" />

<div class="row">
	<div class="col-lg-3 col-xs-6">
        <div class="small-box bg-aqua">
            <div class="inner">
            	<a href="<?= admin_url('orders') ?>" class="text-white">
	              	<h3><?= $open_order ?></h3>
	              	<p>Open Orders</p>
	            </a>
            </div>
            <div class="icon">
              	<i class="fa fa-star"></i>
            </div>
        </div>
    </div>

	<div class="col-lg-3 col-xs-6">
        <div class="small-box bg-blue">
            <div class="inner">
            	<a href="<?= admin_url('orders') ?>" class="text-white">
	              	<h3><?= $total_order ?></h3>
	              	<p>Total Orders</p>
	            </a>
            </div>
            <div class="icon">
              	<i class="fa fa-cubes"></i>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-green">
            <div class="inner">
              	<h3><?= $total_revenue ?></h3>
              	<p>Total Revenue</p>
            </div>
            <div class="icon">
              	<i class="fa fa-money"></i>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-xs-6">
        <div class="small-box bg-red">
            <div class="inner">
                <a href="<?= admin_url('orders') ?>" class="text-white">
                  	<h3><?= $returns ?></h3>
                  	<p>Refunded</p>
                </a>
            </div>
            <div class="icon">
              	<i class="fa fa-undo"></i>
            </div>
        </div>
    </div>
</div>

<div class="row">
	<div class="col-lg-6">
		<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Order By Date</h3>
            </div>
			<div class="box-body chart-responsive">
	            <div class="chart" id="order-chart" style="height: 300px;"></div>
	        </div>
	    </div>
	</div>
	<div class="col-lg-6">
		<div class="box box-info">
            <div class="box-header with-border">
              	<h3 class="box-title">Revenue By Date</h3>
            </div>
			<div class="box-body chart-responsive">
	            <div class="chart" id="revenue-chart" style="height: 300px;"></div>
	        </div>
	    </div>
	</div>
</div>

<link rel="stylesheet" href="<?= assets('css/daterangepicker.css') ?>" />
<link rel="stylesheet" href="<?= assets('css/morris.min.css') ?>" />
<script src="<?= assets('js/moment.min.js') ?>"></script>
<script src="<?= assets('js/daterangepicker.js') ?>"></script>
<script src="<?= assets('js/raphael.min.js') ?>"></script>
<script src="<?= assets('js/morris.min.js') ?>"></script>
<script type="text/javascript">
$(function () {
	$('#daterange-btn').daterangepicker({
		"maxSpan": {
	        "days": 31
	    },
        ranges   : {
          	'Today'       : [moment(), moment()],
          	'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          	'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          	'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          	'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          	'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: '<?= date('m/d/Y', strtotime($start_date)) ?>',
        endDate  : '<?= date('m/d/Y', strtotime($end_date)) ?>'
    }, function (start, end) {
    	$('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    	window.location.href = "<?= admin_url('dashboard') ?>?date=" + start.format('YYYY-MM-D') + '|' + end.format('YYYY-MM-DD');
    })

	new Morris.Line({
      	element: 'order-chart',
      	resize: true,
      	data: <?= json_encode($orders_by_date) ?>,
      	xkey: 'y',
      	ykeys: ['orders'],
      	labels: ['Order'],
      	lineColors: ['#3c8dbc'],
      	hideHover: 'auto'
    });

    new Morris.Line({
      	element: 'revenue-chart',
      	resize: true,
      	data: <?= json_encode($revenue_by_date) ?>,
      	xkey: 'y',
      	ykeys: ['revenue'],
      	labels: ['Revenue'],
      	lineColors: ['#3c8dbc'],
      	hideHover: 'auto'
    });
});
</script>