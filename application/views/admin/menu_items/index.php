<div class="box box-primary">
    <div class="box-body">
        <p class="clearfix">
            <a class="btn btn-default" href="<?= admin_url('menu') ?>">Back</a>
            <a class="btn btn-primary pull-right" href="<?= admin_url('menuitems/create/' . $id) ?>">Add New</a>
            <a class="btn btn-warning pull-right" href="<?= admin_url('menuitems/hierarchy/' . $id) ?>" style="margin-right: 20px;"><i class="fa fa-sitemap"></i> Hierarchical View</a>
        </p>
        
        <hr />

        <table id="menu_items-table" class="table table-responsive">
            <thead>
                <tr>
                    <th width="5%" class="text-center">ID</th>
                    <th>Label</th>
                    <th>Page/Custom Link</th>
                    <th>Status</th>
                    <th width="10%" class="text-right">Action</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>
</div>

<script type="application/javascript">
$(function () {
    $('#menu_items-table').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax":{
            "url": "<?= admin_url('menuitems/datatable/' . $id) ?>",
            "dataType": "json",
            "type": "POST",
        },
        columnDefs: [
            {targets: [0], className: "text-center"},
            {targets: -1, orderable: false, className: "text-right"}
        ]
    });
})
</script>