<div class="box box-primary">
    <div class="box-body">
        <?= form_open(null, array('id' => 'menu-frm')) ?>
            <input type="hidden" name="data" value="" id="data" />

            <div id="nestable" class="dd">
                <ol class="dd-list">
                    <?= $html ?>
                </ol>
            </div>

            <div class="row">
                <div class="form-group col-sm-12">
                    <button type="button" id="submit-btn" name="submit-btn" class="btn btn-primary">Save</button> 
                    <a href="<?= admin_url('menuitems/index/' . $id) ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('#nestable').nestable({
        maxDepth: 1
    });

    $('#submit-btn').click(function(){
        var data = window.JSON.stringify($('#nestable').nestable('serialize'));
        $('#data').val(data);
        $('#menu-frm').submit();
    });
});
</script>