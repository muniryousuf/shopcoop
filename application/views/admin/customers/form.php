<div class="box box-primary">
    <div class="box-body">
        <?= form_open() ?>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="first_name" class="required">First Name</label>
                    <input type="text" id="first_name" name="first_name" class="form-control" value="<?= isset($row['first_name']) ? $row['first_name'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="last_name" >Last Name</label>
                    <input type="text" id="last_name" name="last_name" class="form-control" value="<?= isset($row['last_name']) ? $row['last_name'] : "" ?>"  />
                </div>

                <div class="form-group col-sm-6">
                    <label for="email" class="required">Email</label>
                    <input type="text" id="email" name="email" class="form-control" value="<?= isset($row['email']) ? $row['email'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="password">New Password</label>
                    <input type="password" id="password" name="password" class="form-control" <?= isset($row['id']) && $row['id'] ? '' : 'required' ?> />
                    <small>(Leave blank if you don't want to change)</small>
                </div>

                <div class="col-sm-12">
                    <hr />
                </div>

                <div class="col-sm-6">
                    <h4>Billing Address:</h4>

                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="billing_first_name">Voornaam <span class="required">*</span></label>
                            <input type="text" id="billing_first_name" name="billing[first_name]" class="form-control" value="<?= isset($row['billing']['first_name']) ? $row['billing']['first_name'] : '' ?>" maxlength="50" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="billing_last_name">Achternaam <span class="required">*</span></label>
                            <input type="text" id="billing_last_name" name="billing[last_name]" class="form-control" value="<?= isset($row['billing']['last_name']) ? $row['billing']['last_name'] : '' ?>" maxlength="50" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="billing_country">Land <span class="required">*</span></label>
                        <input type="text" id="billing_country" class="form-control" value="<?= isset($row['billing']['country']) ? $row['billing']['country'] : '' ?>" readonly />
                    </div>

                    <div class="form-group">
                        <label for="billing_address1">Straat en huisnummer <span class="required">*</span></label>
                        <input type="text" id="billing_address1" name="billing[address1]" class="form-control" placeholder="Straatnaam en huisnummer" value="<?= isset($row['billing']['address1']) ? $row['billing']['address1'] : '' ?>" maxlength="255" />
                        <input type="text" id="billing_address2" name="billing[address2]" class="form-control" placeholder="Appartement, suite, unit etc. (optioneel)" maxlength="255" value="<?= isset($row['billing']['address2']) ? $row['billing']['address2'] : '' ?>" style="margin-top: 10px" />
                    </div>

                    <div class="form-group">
                        <label for="billing_postalcode">Postcode <span class="required">*</span></label>
                        <input type="text" id="billing_postalcode" name="billing[postalcode]" class="form-control" value="<?= isset($row['billing']['postalcode']) ? $row['billing']['postalcode'] : '' ?>" maxlength="10" />
                    </div>

                    <div class="form-group">
                        <label for="billing_city">Plaats <span class="required">*</span></label>
                        <input type="text" id="billing_city" name="billing[city]" class="form-control" value="<?= isset($row['billing']['city']) ? $row['billing']['city'] : '' ?>" maxlength="50" />
                    </div>
                </div>

                <div class="col-sm-6">
                    <h4>Shipping Address:</h4>

                    <div class="row">
                        <div class="col-md-6 form-group">
                            <label for="shipping_first_name">Voornaam <span class="required">*</span></label>
                            <input type="text" id="shipping_first_name" name="shipping[first_name]" class="form-control" value="<?= isset($row['shipping']['first_name']) ? $row['shipping']['first_name'] : '' ?>" maxlength="50" />
                        </div>
                        <div class="col-md-6 form-group">
                            <label for="shipping_last_name">Achternaam <span class="required">*</span></label>
                            <input type="text" id="shipping_last_name" name="shipping[last_name]" class="form-control" value="<?= isset($row['shipping']['last_name']) ? $row['shipping']['last_name'] : '' ?>" maxlength="50" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="shipping_country">Land <span class="required">*</span></label>
                        <input type="text" id="shipping_country" class="form-control" value="<?= isset($row['shipping']['country']) ? $row['shipping']['country'] : '' ?>" readonly />
                    </div>

                    <div class="form-group">
                        <label for="shipping_address1">Straat en huisnummer <span class="required">*</span></label>
                        <input type="text" id="shipping_address1" name="shipping[address1]" class="form-control" placeholder="Straatnaam en huisnummer" value="<?= isset($row['shipping']['address1']) ? $row['shipping']['address1'] : '' ?>" maxlength="255" />
                        <input type="text" id="shipping_address2" name="shipping[address2]" class="form-control" placeholder="Appartement, suite, unit etc. (optioneel)" maxlength="255" value="<?= isset($row['shipping']['address2']) ? $row['shipping']['address2'] : '' ?>" style="margin-top: 10px" />
                    </div>

                    <div class="form-group">
                        <label for="shipping_postalcode">Postcode <span class="required">*</span></label>
                        <input type="text" id="shipping_postalcode" name="shipping[postalcode]" class="form-control" value="<?= isset($row['shipping']['postalcode']) ? $row['shipping']['postalcode'] : '' ?>" maxlength="10" />
                    </div>

                    <div class="form-group">
                        <label for="shipping_city">Plaats <span class="required">*</span></label>
                        <input type="text" id="shipping_city" name="shipping[city]" class="form-control" value="<?= isset($row['shipping']['city']) ? $row['shipping']['city'] : '' ?>" maxlength="50" />
                    </div>
                </div>                

                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button> 
                    <a href="<?= admin_url('customers') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>