<div class="box box-primary">
    <div class="box-body">
        <?= form_open() ?>
            <div class="row">
                <div class="form-group col-sm-6">
                    <label for="title" class="required">Title</label>
                    <input type="text" id="title" name="title" class="form-control" value="<?= isset($row['title']) ? $row['title'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="title" class="required">Slug</label>
                    <input type="text" id="slug" name="slug" class="form-control" value="<?= isset($row['slug']) ? $row['slug'] : "" ?>" required />
                </div>

                <div class="form-group col-sm-6">
                    <label for="status" class="required">Status</label>
                    <select id="status" name="status" class="form-control">
                        <option value="1" <?= isset($row['status']) && $row['status'] == 1 ? 'selected' : '' ?>>Published</option>
                        <option value="0" <?= isset($row['status']) && $row['status'] == 0 ? 'selected' : '' ?>>Draft</option>
                    </select>
                </div>

                <div class="form-group col-sm-6">
                    <label for="template" >Template</label>
                    <select id="template" name="template" class="form-control">
                        <option value="">Default</option>
                        <option value="page-breadcrumb" <?= isset($row['template']) && $row['template'] == 'page-breadcrumb' ? 'selected' : '' ?>>Title & Breadcrumb</option>
                        <option value="faqs" <?= isset($row['template']) && $row['template'] == 'faqs' ? 'selected' : '' ?>>FAQs</option>
                        <option value="shop" <?= isset($row['template']) && $row['template'] == 'shop' ? 'selected' : '' ?>>Shop</option>
                        <option value="cart" <?= isset($row['template']) && $row['template'] == 'cart' ? 'selected' : '' ?>>Cart</option>
                        <option value="checkout" <?= isset($row['template']) && $row['template'] == 'checkout' ? 'selected' : '' ?>>Checkout</option>
                        <option value="thankyou" <?= isset($row['template']) && $row['template'] == 'thankyou' ? 'selected' : '' ?>>Thank You</option>
                        <option value="account" <?= isset($row['template']) && $row['template'] == 'account' ? 'selected' : '' ?>>Customer Account</option>
                        <option value="wishlist" <?= isset($row['template']) && $row['template'] == 'wishlist' ? 'selected' : '' ?>>Wishlist</option>
                        <option value="blog-posts" <?= isset($row['template']) && $row['template'] == 'blog-posts' ? 'selected' : '' ?>>Blog Posts</option>
                    </select>
                </div>

                <div class="form-group col-sm-12">
                    <label for="content" >Content</label>
                    <textarea id="content" name="content" class="form-control editor" ><?= isset($row['content']) ? $row['content'] : "" ?></textarea>
                </div>

                <div class="col-sm-12">
                    <div class="well">
                        <h3 class="meta-heading">META DATA:</h3>
                        <div class="row">
                            <div class="form-group col-sm-12">
                                <label for="meta_title" >Meta Title</label>
                                <input type="text" id="meta_title" name="meta_title" class="form-control" value="<?= isset($row['meta_title']) ? $row['meta_title'] : "" ?>"  />
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="meta_desc" >Meta Desc</label>
                                <textarea id="meta_desc" name="meta_desc" class="form-control"><?= isset($row['meta_desc']) ? $row['meta_desc'] : "" ?></textarea>
                            </div>

                            <div class="form-group col-sm-12">
                                <label for="index_follow">Robots Tag</label>
                                <select id="index_follow" name="index_follow" class="form-control">
                                    <option value="">Index, Follow</option>
                                    <option value="index,nofollow" <?= isset($row['index_follow']) && $row['index_follow'] == 'index,nofollow' ? 'selected' : '' ?>>Index, NoFollow</option>
                                    <option value="noindex,follow" <?= isset($row['index_follow']) && $row['index_follow'] == 'noindex,follow' ? 'selected' : '' ?>>NoIndex, Follow</option>
                                    <option value="noindex,nofollow" <?= isset($row['index_follow']) && $row['index_follow'] == 'noindex,nofollow' ? 'selected' : '' ?>>NoIndex, NoFollow</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button> 
                    <a href="<?= admin_url('pages') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>

<script type="application/javascript">
$(function(){
    if($("#slug").val() == '') {
        $("#title").on('input',function(e){
            $("#slug").val(slugify($(this).val()));
        });
    }
})
</script>