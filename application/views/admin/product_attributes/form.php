<div class="box box-primary">
    <div class="box-body">
        <?= form_open('', ['id' => 'attrFrm']) ?>
            <div class="row">
                <div class="form-group col-sm-12">
                    <label for="name" class="required">Name</label>
                    <input type="text" id="name" name="name" class="form-control" value="<?= isset($row['name']) ? $row['name'] : "" ?>" required />
                </div>

                <div class="col-sm-12">
                    <table id="values-tbl" class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Values <span style="color: red">*</span></th>
                                <th width="5%"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if(isset($row['values'])) { ?>
                                <?php foreach ($row['values'] as $k => $v) { ?>
                                <tr>
                                    <td><input type="text" name="values[<?= $k ?>][name]" value="<?= $v['name'] ?>" class="form-control" required /></td>
                                    <td align="right">
                                        <a href="javascript:void(0)" class="btn btn-sm btn-danger remove-value">x</a>
                                        <input type="hidden" name="values[<?= $k ?>][id]" value="<?= $v['id'] ?>" />
                                    </td>
                                </tr>
                                <?php } ?>
                            <?php } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="2"><a href="javascript:void(0)" class="btn btn-primary add-value">+ Add New Value</a></td>
                            </tr>
                        </tfoot>                
                    </table>
                </div>

                <div class="form-group col-sm-12">
                    <button type="submit" name="submit" class="btn btn-primary">Save</button> 
                    <a href="<?= admin_url('productattributes') ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        <?= form_close() ?>
    </div>
</div>

<script type="text/javascript">
var vcnt = <?= isset($row['values']) ? count($row['values']) : 0; ?>;

$(function(){
    $('#attrFrm').validate();

    if($("#slug").val() == '') {
        $("#name").on('input', function(e){
            $("#slug").val(slugify($(this).val()));
        });
    }

    $('.add-value').click(function(){
        var tr  = '<tr>';
            tr += '  <td><input type="text" name="values[' + vcnt + '][name]" value="" class="form-control" required /></td>';
            tr += '  <td align="right">';
            tr += '    <a href="javascript:void(0)" class="btn btn-sm btn-danger remove-value">x</a>';
            tr += '    <input type="hidden" name="values[' + vcnt + '][id]" value="" />';
            tr += '  </td>';
            tr += '</tr>';


        $('#values-tbl tbody').append(tr);
        vcnt++;
    });

    $(document).on('click', '.remove-value', function(){
        if(confirm('Are you sure you want to delete this?')) {
            $(this).parents('tr').remove();
        }
    });
})
</script>