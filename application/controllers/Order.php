<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends MY_Controller {

	function __construct()
    {

        parent::__construct();

        $this->load->model('Customers_model', 'Customers');
        $this->load->model('Orders_model', 'Orders');
        $this->load->model('Order_address_model', 'OrderAddress');
        $this->load->model('Order_products_model', 'OrderProducts');
    }

    public function process()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $cart   = $this->getCart();
            $inputs = $this->input->post();
            $mollie = mollie_client();

            $this->form_validation->set_rules('billing[first_name]', 'Voornaam', 'required');
            $this->form_validation->set_rules('billing[last_name]', 'Achternaam', 'required');
            $this->form_validation->set_rules('billing[address1]', 'Straat en huisnummer', 'required');
            $this->form_validation->set_rules('billing[postalcode]', 'Postcode', 'required');
            $this->form_validation->set_rules('billing[city]', 'Plaats', 'required');
            $this->form_validation->set_rules('email', 'E-mailadres', 'required|valid_email');
            $this->form_validation->set_rules('payment_method', 'Payment Method', 'required');

            if(isset($inputs['create_account']) && $inputs['create_account'])
            {
                $this->form_validation->set_rules('email', 'Accountwachtwoord aanmaken', 'required|valid_email|callback_validate_unique_email', ['validate_unique_email' => 'Email address already registered.']);
                $this->form_validation->set_rules('account_password', 'Accountwachtwoord aanmaken', 'required');
            }

            if(isset($inputs['new_shipping_address']) && $inputs['new_shipping_address'])
            {
                $this->form_validation->set_rules('shipping[first_name]', 'Shipping Voornaam', 'required');
                $this->form_validation->set_rules('shipping[last_name]', 'Shipping Achternaam', 'required');
                $this->form_validation->set_rules('shipping[address1]', 'Shipping Straat en huisnummer', 'required');
                $this->form_validation->set_rules('shipping[postalcode]', 'Shipping Postcode', 'required');
                $this->form_validation->set_rules('shipping[city]', 'Shipping Plaats', 'required');
            }

            $this->form_validation->set_data($inputs);

            if ($this->form_validation->run() == FALSE)
            {
                $this->session->set_flashdata('error', '<p>Please fix the below mentioned errors:</p>' . validation_errors('- ', '<br />'));
                redirect(url('/' . $this->vars['checkout_url']));
            }

            // Create account
            $customer_id = 0;
            if(isset($inputs['create_account']) && $inputs['create_account'])
            {
                $data = [
                    'first_name' => $inputs['billing']['first_name'],
                    'last_name'  => $inputs['billing']['last_name'],
                    'email'      => $inputs['email'],
                    'password'   => md5($inputs['account_password']),
                ];

                $this->Customers->insert($data, false);
                $customer_id = $this->Customers->getLastInsertID();
            }

            // Payment method
            $payment_method_list   = payment_methods();
            $payment_method_id     = $inputs['payment_method'];
            $payment_method_issuer = isset($inputs['issuer'][$inputs['payment_method']]) ? $inputs['issuer'][$inputs['payment_method']] : '';
            $payment_method        = $payment_method_id;

            foreach ($payment_method_list as $pm)
            {
                if($pm['id'] == $payment_method_id)
                {
                    $payment_method = $pm['name'];

                    if($payment_method_issuer && $pm['issuers'])
                    {
                        foreach ($pm['issuers'] as $pmi)
                        {
                            if($pmi->id == $payment_method_issuer)
                            {
                                $payment_method .= ' - ' . $pmi->name;
                            }
                        }
                    }
                }
            }

            // Create order
            $data = [
                'customer_id'    => $customer_id,
                'email'          => $inputs['email'],
                'phone'          => $inputs['phone'],
                'status'         => 0,
                'payment_method' => $payment_method,
                'sub_total'      => $cart['subtotal'],
                'tax'            => $cart['tax'],
                'net_total'      => $cart['nettotal'],
                'comments'       => $inputs['comments'],
            ];

            $this->Orders->insert($data, false);
            $order_id = $this->Orders->getLastInsertID();

            // Order billing address
            $bill_data = $inputs['billing'];
            $bill_data['type'] = 1;
            $bill_data['order_id'] = $order_id;
            $bill_data['country']  = 'Pakistan';

            $this->OrderAddress->insert($bill_data, false);

            // Order shipping address
            if(isset($inputs['new_shipping_address']) && $inputs['new_shipping_address'])
            {
                $ship_data = $inputs['shipping'];
                $ship_data['type'] = 2;
                $ship_data['order_id'] = $order_id;
                $ship_data['country']  = 'Pakistan';

                $this->OrderAddress->insert($ship_data, false);
            }

            // Order products
            foreach ($cart['items'] as $pro)
            {
                $name = $pro['product_name'];


                if($pro['attribute'])
                {
                    $name .= ' - ' . implode(', ', isset($pro['attribute_name'][0])?$pro['attribute_name'][0]:[]);
                }


                $product_data = [
                    'order_id'   => $order_id,
                    'product_id' => $pro['product'],
                    'name'       => $name,
                    'attributes' => 'small',//isset($pro['attribute_name'][0])  ? implode(',', $pro['attribute']):'-',
                    'price'      => $pro['sale_price'] ? $pro['sale_price'] : $pro['price'],
                    'quantity'   => $pro['quantity'],
                ];

                $this->OrderProducts->insert($product_data, false);
            }

            // Create payment
//            $payment = $mollie->payments->create([
//                "amount" => [
//                    "currency" => "EUR",
//                    "value" => "" . number_format($cart['nettotal'], 2, '.', '')
//                ],
//                "description" => $this->vars['site']['site_name'] . ' order payment #' . $order_id,
//                "redirectUrl" => url('/' . $this->vars['thankyou_url'] . '/?orderid=' . $order_id),
//                // "webhookUrl"  => url('/webhook/mollie'),
//                "method"      => $inputs['payment_method'],
//                "issuer"      => isset($inputs['issuer'][$inputs['payment_method']]) ? $inputs['issuer'][$inputs['payment_method']] : '',
//            ]);

            // Update payment id
            $this->Orders->update(['payment_id' => 1], $order_id, false);

            // Clear cart
            $this->session->set_userdata('cart', '');

            // Redirect to payment
            header("Location: " .$this->vars['thankyou_url'], true, 303);
            exit;
        }

        redirect(url('/' . $this->vars['checkout_url']));

    }

    public function validate_unique_email($email)
    {
        $user = $this->Customers->find()->where('email', $email);

        $result = $user->get()->num_rows();

        return $result == 0 ? true : false;
    }

    public function checkPostalCode(){
        $inputs = $this->input->post();

        $inputs['postcode'] = strtolower(str_replace(' ','',trim($inputs['postcode'])));

        $url = $url = "https://maps.googleapis.com/maps/api/distancematrix/json?key=AIzaSyDwX7Hvp5q6fuQ8uwCEYARNwYIgJ-6_3mo&origins=".$inputs['postcode']."&destinations=Nw105hd&mode=driving&language=en-EN&sensor=false";

        $data   = @file_get_contents($url);
        $result = json_decode($data, true);

        if(isset($result["rows"][0]["elements"][0]["distance"])) {

            $distance = array( // converts the units
                "meters" => $result["rows"][0]["elements"][0]["distance"]["value"],
                "kilometers" => $result["rows"][0]["elements"][0]["distance"]["value"] / 1000,
                "yards" => $result["rows"][0]["elements"][0]["distance"]["value"] * 1.0936133,
                "miles" => $result["rows"][0]["elements"][0]["distance"]["value"] * 0.000621371
            );

           $response = [ 'success' => true, 'miles' => $distance['miles']];

        } else {

            $response = [ 'success' => false, "message" => "Please enter a valid post code to place your order "];

        }
        echo json_encode($response);
    }
}
