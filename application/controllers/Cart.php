<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//sumair asim
class Cart extends MY_Controller {

    public function add()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $inputs = $this->input->post();
            $cart   = $this->session->userdata('cart');

            $cart   = $cart ? $cart : [];

            $id             = $inputs['product'];
            $qty            = (int) $inputs['quantity'];
            $qty            = $qty ? $qty : 1;
            $discountItemId = $inputs['QuantityDiscountItem']??NULL;
            $product = $this->Products
                ->find()
                ->where('id', $id)
                ->get()->row_array();


            if($product)
            {
                $key = $id;

                if(isset($inputs['attribute']))
                {
                    $key .= '-' . implode(',', $inputs['attribute']);
                }


                $key = md5($key);

                if(isset($cart[$key]))
                {
                    $cart[$key]['quantity'] = $cart[$key]['quantity'] + $qty;
                }
                else
                {
                    $cart[$key] = [
                        'product'   => $id,
                        'attribute' => !isset($inputs['attribute']) && empty($inputs['attribute']) ? [] : $inputs['attribute'],
                        'quantity'  => $qty,
                        'discountItemId'  => $discountItemId
                    ];
                }



                $this->session->set_userdata('cart', $cart);
                $this->session->set_flashdata('success', '"' . $product['name'] . '" has been added to your shopping cart.');
            }

            redirect(url('/' . $this->vars['cart_url']));
        }

        redirect(url('/'));
    }

    public function update()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $inputs = $this->input->post();
            $cart   = $this->session->userdata('cart');
            $cart   = $cart ? $cart : [];

            foreach ($inputs['quantity'] as $key => $qty)
            {
                if(isset($cart[$key]))
                {
                    $cart[$key]['quantity'] = $qty;
                }
            }

            $this->session->set_userdata('cart', $cart);
            $this->session->set_flashdata('success', 'Shopping cart updated.');
        }

        redirect(url('/' . $this->vars['cart_url']));
    }

    public function remove()
    {
        $key  = $this->input->get('key');
        $cart = $this->session->userdata('cart');

        if($key && isset($cart[$key]))
        {
            $item = $cart[$key];

            $product = $this->Products
                ->find()
                ->where('id', $item['product'])
                ->get()
                ->row_array();

            unset($cart[$key]);

            $this->session->set_flashdata('success', '"' . $product['name'] . '" removed.');
            $this->session->set_userdata('cart', $cart);
        }

        redirect(url('/' . $this->vars['cart_url']));
    }



    public function ajaxCart(){

        $inputs = $this->input->post();
        $cart   = $this->session->userdata('cart');
        $cart   = $cart ? $cart : [];

        $id             = $inputs['product'];
        $qty            = (int) $inputs['quantity'];
        $qty            = $qty ? $qty : 1;
        $discountItemId = $inputs['QuantityDiscountItem']??NULL;
        $product = $this->Products
            ->find()
            ->where('id', $id)
            ->get()->row_array();


        if($product)
        {
            $key = $id;

            if(isset($inputs['attribute']))
            {
                $key .= '-' . implode(',', $inputs['attribute']);
            }


            $key = md5($key);

            if(isset($cart[$key]))
            {
                $cart[$key]['quantity'] = $cart[$key]['quantity'] + $qty;
            }
            else
            {
                $cart[$key] = [
                    'product'   => $id,
                    'attribute' => !isset($inputs['attribute']) && empty($inputs['attribute']) ? [] : $inputs['attribute'],
                    'quantity'  => $qty,
                    'discountItemId'  => $discountItemId
                ];
            }



            $this->session->set_userdata('cart', $cart);
            $cart   = $this->session->userdata('cart');
            echo json_encode($cart);
        }
    }

    public function getCartAjax(){
        $cart = get_cart();
        $html = '';
        $html.= '<ul class="navbar-nav attr-nav align-items-center">';
        $html.= ' <li class="dropdown cart_dropdown">';
        $html.='<a class="nav-link cart_trigger" href="'. cart_url().' "data-toggle="dropdown"  style="padding-left: 35px;" href="#">';
        $html.='<i class="fa fa-shopping-basket" aria-hidden="true"></i>';
        $html.=' <span class="amt">';
        $html.= format_money($cart['nettotal']);
        $html.= '<span class="cart_count">'.count($cart['items']) .'</span>';
        $html.= '</a>';
        if (count($cart['items']) > 0) {
            $html .= '<div class="cart_box dropdown-menu dropdown-menu-right">';
            $html .= '<ul class="cart_list">';
            $total = 0;
            foreach ($cart['items'] as $cart) {
                $html .= '<li>';
                $html .= ' <a href="' . cart_url() . '">';
                $html .= '<img src="' . $cart['image'] . '"alt="cart_thumb1">' . $cart['product_name'];
                $html .= '</a>';
                $html .= '<span class="cart_quantity">' . $cart['quantity'] . 'x' . '<span class="cart_amount"><span class="price_symbole"></span></span>';
                if ($cart['sale_price'] && $cart['sale_price'] > 0) {
                    $html .= '<span class="text_black">' . format_money($cart['sale_price']) . '</span>';
                    $total += ($cart['sale_price'] * $cart['quantity']);
                } else {
                    $html .= '<span class="text_black">' . format_money($cart['price']) . '';
                    $total += ($cart['price'] * $cart['quantity']);
                    $html .= '</span>';

                }
                $html.='</span>';
                $html.='</li>';

            }
            $html.=' </ul>';
            $html.=' <div class="cart_footer">';
            $html.='<p class="cart_total"><strong>Total:</strong> <span class="cart_price"><span class="price_symbole"></span></span>'.format_money($total).' </p>';
            $html.='<p class="cart_buttons"><a href="'.cart_url().'"   class="btn btn-fill-line rounded-0 view-cart mb-2">View Cart</a>';
            $html.='<a href="'.checkout_url().'"  class="btn btn-fill-out rounded-0 checkout ml-0">Checkout</a></p></div>';
            $html.=' </div> </div>';
            $html.='</li></ul>';
            echo $html;
        }
    }


}
