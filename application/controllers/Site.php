<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Site extends MY_Controller
{
    public function index()
    {
        if ($this->uri->segment(1) == 'site') {
            redirect('/');
        }
        $frontpage = $this->vars['site']['frontpage'];
        $page = $this->Pages
            ->find()
            ->where('id', $frontpage)
            ->get()
            ->row_array();
        if ($page) {
            $this->seo = generate_meta($page);
            $view = $page['template'] ? $page['template'] : 'page';
            //$this->load->view('templates/header');
            $this->load->front($view, compact('page'));
            //$this->load->view('templates/footer');
        } else {
            show_404();
        }
    }



    public function dologin()
    {
        if ($this->input->post('action') && $this->input->post('action') == 'login') {
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'password', 'trim|required');

            if ($this->form_validation->run()) {
                $user = $this->Customers
                    ->find()
                    ->where('email', $email)
                    ->where('password', MD5($password))
                    ->get()
                    ->row_array();

                if ($user) {
                    $this->session->set_userdata(array('logged_user' => $user));
                } else {
                    $this->session->set_flashdata('error', 'Invalid e-mail/password.');
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        redirect(account_url());
    }

    public function doregister()
    {
        if ($this->input->post('action') && $this->input->post('action') == 'register') {
            $email = $this->input->post('email');
            $password = $this->input->post('password');

            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email|callback_validate_unique_email');
            $this->form_validation->set_rules('password', 'password', 'trim|required');
            $this->form_validation->set_message('validate_unique_email', 'Er is al een account geregistreerd met je e-mailadres.');

            if ($this->form_validation->run()) {
                $name = explode('@', $email);
                $name = $name[0];

                $data = [
                    'first_name' => $name,
                    'email'      => $email,
                    'password'   => md5($password)
                ];

                $this->Customers->insert($data, false);
                $id = $this->Customers->getLastInsertID();

                $data['title'] = 'Welkom bij ' . site_title();
                $email_body = $this->load->front_email('register', $data);

                $this->load->library('email');

                $this->email->from(NO_REPLY_EMAIL, site_name());
                $this->email->to($data['email']);
                $this->email->subject($data['title']);
                $this->email->message($email_body);
                $this->email->send();

                $user = $this->Customers->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

                $this->session->set_userdata(array('logged_user' => $user));
            } else {
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        redirect(account_url());
    }

    public function dopwdrest()
    {
        if ($this->input->post('action') && $this->input->post('action') == 'forgot-password') {
            $email = $this->input->post('email');

            $this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');

            if ($this->form_validation->run()) {
                $user = $this->Customers
                    ->find()
                    ->where('email', $email)
                    ->get()
                    ->row_array();

                if ($user) {
                    $reset_key = md5(time());
                    $this->Customers->update(['reset_key' => $reset_key], $user['id'], false);

                    $data = array(
                        'title'      => 'Aanvraag voor wachtwoordherstel',
                        'subject'    => 'Aanvraag voor wachtwoordherstel voor ' . site_title(),
                        'first_name' => $user['first_name'],
                        'email'      => $user['email'],
                        'reset_url'  => account_url() . '/?reset_password=1&email=' . $user['email'] . '&code=' . $reset_key
                    );

                    $email_body = $this->load->front_email('reset_password', $data);

                    $this->load->library('email');

                    $this->email->from(NO_REPLY_EMAIL, site_name());
                    $this->email->to($user['email']);
                    $this->email->subject($data['subject']);
                    $this->email->message($email_body);
                    $this->email->send();

                    $this->session->set_flashdata('success', 'Een wachtwoord reset-email is naar je geregistreerde e-mailadres gestuurd, maar het kan enkele minuten duren voor deze in je inbox verschijnt. Wacht minimaal 10 minuten voor je nog een poging tot resetten waagt.');
                    redirect(account_url());
                } else {
                    $this->session->set_flashdata('error', 'Ongeldige e-mailadres.');
                }
            } else {
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        redirect(account_url() . '/?lost-password=1');
    }

    public function dopwdupdate()
    {
        if ($this->input->post('action') && $this->input->post('action') == 'reset-password') {
            $email = $this->input->post('email');
            $code = $this->input->post('code');

            $user = $this->Customers
                ->find()
                ->where('email', $email)
                ->where('reset_key', $code)
                ->get()
                ->row_array();

            if ($user) {
                $password = $this->input->post('password');
                $repassword = $this->input->post('repassword');

                $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[8]');
                $this->form_validation->set_rules('repassword', 'confirm password', 'trim|required|matches[password]');

                if ($this->form_validation->run()) {
                    $this->Customers->update([
                        'password'  => md5($password),
                        'reset_key' => '',
                    ], $user['id'], false);

                    $this->session->set_flashdata('success', 'Wachtwoord succesvol bijgewerkt.');
                } else {
                    $this->session->set_flashdata('error', validation_errors());
                    redirect(account_url() . '/?reset_password=1&email=' . $email . '&code=' . $code);
                }
            } else {
                $this->session->set_flashdata('error', 'Kan uw verzoek niet verwerken, link om wachtwoord opnieuw in te stellen is ongeldig of verlopen.');
            }
        }

        redirect(account_url());
    }

    public function logout()
    {
        $this->session->unset_userdata('logged_user');
        $this->session->set_flashdata('success', 'Successfully Logged Out...');
        redirect(account_url());
    }

    public function validate_unique_email($email)
    {
        $user = $this->Customers->find()->where('email', $email);

        $result = $user->get()->num_rows();

        return $result == 0 ? true : false;
    }
}