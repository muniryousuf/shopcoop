<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends Admin_Controller {

    private $dataTableColumns = ["id","title","status","created_at"];
    private $dateFields = ["created_at","updated_at"];

    function __construct()
    {
        parent::__construct();

        $this->load->model('Pages_model', 'Pages');
        $this->load->model('Seo_urls_model', 'SeoUrl');
        $this->pageTitle = 'Pages';
    }

    public function index()
    {
        $this->load->admin('pages/index');
    }

    public function create()
    {
        $row = $this->_save();

        $this->load->admin('pages/form', compact('row'));
    }

    public function update($id)
    {
        $row = $this->_load($id);

        $url = $this->SeoUrl
                    ->find()
                    ->where('row_id', $id)
                    ->where('tbl_name', 'pages')
                    ->get()
                    ->row_array();

        $row['slug'] = $url['slug'];

        $row = $this->_save($row);

        $this->load->admin('pages/form', compact('row'));
    }

    public function delete($id)
    {
        $row = $this->_load($id);

        $this->Pages->delete($id);

        $this->session->set_flashdata('success', 'Record deleted successfully.');
        redirect(admin_url('pages'));
    }

    public function datatable()
    {
        $model = $this->Pages->find();
        $totalData = $totalFiltered = $this->Pages->count();
        $model = $this->Pages;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $order = $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');
            
            foreach ($this->dataTableColumns as $c)
            {
                if (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';
            $model->find()->where($where);

            $totalFiltered = $model->count();
        }

        $allData = $model->find()
                        ->where($where)
                        ->limit($limit, $start)
                        ->order_by($order, $dir)
                        ->get()
                        ->result_array();

        $data = array();

        if(!empty($allData))
        {
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    if($c == 'status')
                    {
                        switch ($d[$c]) {
                            case 0:
                                $row[] = '<span class="label label-default">Draft</span>';
                                break;
                            case 1:
                                $row[] = '<span class="label label-success">Published</span>';
                                break;
                        }
                    }
                    elseif(in_array($c, $this->dateFields))
                    {
                        $row[] = date('d-M-Y h:ia', strtotime($d[$c]));
                    }
                    else
                    {
                        $row[] = $d[$c];
                    }
                }

                $update = in_array('pages.update', $this->permissions) ? admin_url('pages/update/' . $d['id']) : '';
                $delete = in_array('pages.delete', $this->permissions) ? admin_url('pages/delete/' . $d['id']) : '';

                $actions = "<div class='btn-group'>";
                
                if($update)
                {
                    $actions .= "  <a href='{$update}' class='btn btn-primary btn-sm' title='Edit'><i class='fa fa-pencil'></i></a>";
                }

                if($delete)
                {
                    $actions .= "  <a href='{$delete}' class='btn btn-danger btn-sm' title='Delete' onclick='return confirm(\"Are you sure you want to delete this?\")'><i class='fa fa-trash'></i></button>";
                }

                $actions .= "</div>";

                $row[] = ($update || $delete) ? $actions : '';
                
                $data[] = $row;
            }
        }

        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->Pages
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('pages'));
        }

        return $row;
    }

    private function _save(&$row = array())
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->library('form_validation');

            $inputs = $this->input->post();
            $slug = $inputs['slug'];

            if ($this->Pages->validate($inputs))
            {
                if(isset($row['id']) && $row['id'])
                {
                    $this->Pages->update($inputs, $row['id'], false);
                }
                else
                {
                    $this->Pages->insert($inputs, false);
                    $row['id'] = $this->Pages->getLastInsertID();
                }

                $seo_url = $this->SeoUrl
                                ->find()
                                ->where('row_id', $row['id'])
                                ->where('tbl_name', 'pages')
                                ->get()
                                ->row_array();

                $slug_arr = array(
                    'slug' => $slug,
                    'tbl_name' => 'pages',
                    'row_id' => $row['id']
                );

                if($seo_url)
                {
                    $this->SeoUrl->update($slug_arr, $seo_url['id'], false);
                }
                else
                {
                    $this->SeoUrl->insert($slug_arr, false);
                }

                $this->session->set_flashdata('success', 'Record saved successfully.');
                redirect(admin_url('pages'));
            }
            else
            {
                $row = array_merge($row, $inputs);
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        return $row;
    }

    public function validate_slug($slug)
    {   
        $id = $this->uri->segment('4');
        
        $row = $this->SeoUrl
                    ->find()
                    ->where('slug', $slug)
                    ->where_in('tbl_name', ['blogs', 'pages']);

        $result = $row->get()->row_array();

        if($result)
        {
            return ($result['tbl_name'] == 'pages' && $result['row_id'] == $id) ? true : false;
        }
        else
        {
            return true;
        }
    }
}
