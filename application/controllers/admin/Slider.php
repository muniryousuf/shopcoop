<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider extends Admin_Controller {

    private $dataTableColumns = ["id","name","slug","status","created_at"];
    private $dateFields = ["created_at","updated_at"];
    function __construct()
    {
        parent::__construct();
        $this->load->model('Sliders_model', 'Sliders');
        $this->load->model('Slider_details_model', 'Slider_Details');
        $this->pageTitle = 'Slider';
    }

    public function index()
    {
        $this->load->admin('slider/index');
    }

    public function create()
    {
        $row = $this->_save();

        $this->load->admin('slider/form', compact('row'));
    }

    public function update($id)
    {
        $row = $this->_load($id);
        $row['images'] = $this->Slider_Details->find()
            ->where('slider_id = ' . $id)
            ->get()
            ->result_array();
        $row = $this->_save($row);
        $this->load->admin('slider/form', compact('row'));
    }

    public function delete($id)
    {
        $row = $this->_load($id);
        $this->Sliders->delete($id);
        $this->session->set_flashdata('success', 'Record deleted successfully.');
        redirect(admin_url('slider'));
    }

    public function datatable()
    {
        $model = $this->Sliders->find();
        $totalData = $totalFiltered = $this->Sliders->count();
        $model = $this->Sliders;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $order = $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');

            foreach ($this->dataTableColumns as $c)
            {
                if (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';
            $model->find()->where($where);

            $totalFiltered = $model->count();
        }

        $allData = $model->find()
                        ->where($where)
                        ->limit($limit, $start)
                        ->order_by($order, $dir)
                        ->get()
                        ->result_array();


        $data = array();



        if(!empty($allData))
        {
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    if($c == 'status')
                    {
                        switch ($d[$c]) {
                            case 0:
                                $row[] = '<span class="label label-default">Draft</span>';
                                break;
                            case 1:
                                $row[] = '<span class="label label-success">Published</span>';
                                break;
                        }
                    }
                    elseif(in_array($c, $this->dateFields))
                    {
                        $row[] = date('d-M-Y h:ia', strtotime($d[$c]));
                    }
                    else
                    {
                        $row[] = $d[$c];
                    }
                }

                $update = in_array('slider.update', $this->permissions) ? admin_url('slider/update/' . $d['id']) : '';
                $delete = in_array('slider.delete', $this->permissions) ? admin_url('slider/delete/' . $d['id']) : '';

                $actions = "<div class='btn-group'>";

                if($update)
                {
                    $actions .= "  <a href='{$update}' class='btn btn-primary btn-sm' title='Edit'><i class='fa fa-pencil'></i></a>";
                }

                if($delete)
                {
                    $actions .= "  <a href='{$delete}' class='btn btn-danger btn-sm' title='Delete' onclick='return confirm(\"Are you sure you want to delete this?\")'><i class='fa fa-trash'></i></button>";
                }

                $actions .= "</div>";

                $row[] = ($update || $delete) ? $actions : '';
                
                $data[] = $row;
            }
        }


        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->Sliders
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('slider'));
        }

        return $row;
    }

    private function _save(&$row = array())
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->library('form_validation');
            $inputs = $this->input->post();

                if(isset($row['id']) && $row['id'])
                {
                    $this->Sliders->update($inputs, $row['id'], false);
                    $this->delete_all_images_record($row['id']);
                    if(isset($inputs['images']) && count($inputs['images']) >0) {
                        foreach ($inputs['images'] as $value){
                            $insert_array   = array('slider_id'=>$row['id'],'image'=>$value['image'],'alt_txt'=>$value['alt_txt'],'text'=>'Text','sort'=>$value['sort_order'],'slider_url'=>$value['slider_url']);
                            $this->Slider_Details->insert($insert_array, false);
                        }
                    }
                }
                else
                {

                    $this->Sliders->insert($inputs, false);
                    $row['id'] = $this->Sliders->getLastInsertID();
                    if(isset($inputs['images']) && count($inputs['images']) >0) {
                        foreach ($inputs['images'] as $value){
                            $insert_array   = array('slider_id'=>$row['id'],'image'=>$value['image'],'alt_txt'=>$value['alt_txt'],'text'=>'Text','sort'=>$value['sort_order'],'slider_url'=>$value['slider_url']);
                            $this->Slider_Details->insert($insert_array, false);
                        }
                    }
                }

                $this->session->set_flashdata('success', 'Record saved successfully.');
                redirect(admin_url('slider'));
        }

        return $row;
    }

    public function validate_slug($slug)
    {   
        $id = $this->uri->segment('4');
        
        $row = $this->SeoUrl
                    ->find()
                    ->where('slug', $slug)
                    ->where_in('tbl_name', ['blogs', 'pages']);

        $result = $row->get()->row_array();

        if($result)
        {
            return ($result['tbl_name'] == 'pages' && $result['row_id'] == $id) ? true : false;
        }
        else
        {
            return true;
        }
    }
    public function delete_all_images_record($id){
        $this->Slider_Details ->find()
            ->where('slider_id='.$id)
            ->delete();
        return;
    }
}
