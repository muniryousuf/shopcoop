<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Admin_Controller {

    private $dataTableColumns = ["id", "name", "email", "net_total", "status", "created_at"];
    private $dateFields = ["created_at","updated_at"];

    function __construct()
    {
        parent::__construct();

        $this->load->model('Orders_model', 'Orders');
        $this->load->model('Order_address_model', 'OrderAddress');
        $this->load->model('Order_products_model', 'OrderProducts');

        $this->pageTitle = 'Orders';
    }

    public function index()
    {
        $this->load->admin('orders/index');
    }

    public function view($id)
    {
        $order = $this->_load($id);

        if($this->input->server('REQUEST_METHOD') == 'POST' && $this->input->post('status'))
        {
            $this->Orders->update(['status' => (int) $this->input->post('status')], $id, false);

            $this->session->set_flashdata('success', 'Status updated successfully.');
            redirect(admin_url('orders/view/' . $id));
        }

        $address = $this->OrderAddress
            ->find()
            ->where('order_id', $id)
            ->get()
            ->result_array();

        $products = $this->OrderProducts
            ->find()
            ->where('order_id', $id)
            ->get()
            ->result_array();

        $next = $this->Orders
            ->find()
            ->where('id > ' . $id)
            ->order_by('id ASC')
            ->get()
            ->row_array();

        $prev = $this->Orders
            ->find()
            ->where('id < ' . $id)
            ->order_by('id DESC')
            ->get()
            ->row_array();

        $next_id = $next ? $next['id'] : 0;
        $prev_id = $prev ? $prev['id'] : 0;

        $this->load->admin('orders/view', compact('order', 'address', 'products', 'next_id', 'prev_id'));
    }

    public function invoice($id)
    {
        $this->load->library('pdf');
        $this->pdf->order_invoice($id);
    }

    public function packingslip($id)
    {
        $this->load->library('pdf');
        $this->pdf->packing_slip($id);
    }

    public function datatable()
    {
        $model = $this->Orders->find();
        $totalData = $totalFiltered = $this->Orders->count();
        $model = $this->Orders;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $pfx = $this->dataTableColumns[$this->input->post('order[0][column]')] == 'name' ? 'a.' : 'o.';
        $order = $pfx . $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');

            foreach ($this->dataTableColumns as $c)
            {
                if($c == 'name')
                {
                    $where[] = 'CONCAT(a.first_name, " ", a.last_name) LIKE "%' . $search . '%"';
                }
                elseif (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(o.' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = 'o.' . $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';
            $model->setAlias('o')
                ->find()
                ->join('order_address AS a', 'o.id = a.order_id AND a.type = 1')
                ->where($where);

            $totalFiltered = $model->count();
        }

        $allData = $model->setAlias('o')
            ->find()
            ->select('o.*, CONCAT(a.first_name, " ", a.last_name) AS name')
            ->join('order_address AS a', 'o.id = a.order_id AND a.type = 1')
            ->where($where)
            ->limit($limit, $start)
            ->order_by($order, $dir)
            ->get()
            ->result_array();

        $data = array();

        if(!empty($allData))
        {
            $statuses = order_statuses();
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    if($c == 'status')
                    {
                        $row[] = isset($statuses[$d[$c]]) ? $statuses[$d[$c]] : 'N/A';
                    }
                    elseif(in_array($c, $this->dateFields))
                    {
                        $row[] = date('d-M-Y h:ia', strtotime($d[$c]));
                    }
                    else
                    {
                        $row[] = $d[$c];
                    }
                }

                $view = in_array('orders.view', $this->permissions) ? admin_url('orders/view/' . $d['id']) : '';

                $actions = "<div class='btn-group'>";

                if($view)
                {
                    $actions .= "  <a href='{$view}' class='btn btn-default btn-sm' title='View'><i class='fa fa-eye'></i></a>";
                }

                $actions .= "</div>";

                $row[] = $view ? $actions : '';

                $data[] = $row;
            }
        }

        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->Orders
            ->find()
            ->where('id', $id)
            ->get()
            ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('orders'));
        }

        return $row;
    }
}
