<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menuitems extends Admin_Controller {

    private $dataTableColumns = ["id","label","page_id","status"];
    private $dateFields = ["created_at","updated_at"];

    function __construct()
    {
        parent::__construct();

        $this->load->model('Menu_model', 'Menu');
        $this->load->model('Menu_items_model', 'Menu_items');
        $this->load->model('Pages_model', 'Pages');
        $this->pageTitle = 'Menu Items';
    }

    public function index($id)
    {
        $menu = $this->_loadMenu($id);
        $this->pageTitle = 'Menu Items - ' . $menu['name'];

        $this->load->admin('menu_items/index', compact('id', 'menu'));
    }

    public function create($id)
    {
        $menu = $this->_loadMenu($id);
        $this->pageTitle = 'Menu Items - ' . $menu['name'];

        $row = array();
        $row = $this->_save($row, $id);

        $pages = $this->Pages
                    ->find()
                    ->get()
                    ->result_array();

        $parents = $this->Menu_items
                    ->find()
                    ->where('menu_id', $id)
                    ->get()
                    ->result_array();

        $this->load->admin('menu_items/form', compact('menu', 'row', 'pages', 'parents'));
    }

    public function update($id)
    {
        $row = $this->_load($id);

        $menu = $this->_loadMenu($row['menu_id']);
        $this->pageTitle = 'Menu Items - ' . $menu['name'];

        $row = $this->_save($row, $row['menu_id']);
        
        $pages = $this->Pages
                    ->find()
                    ->get()
                    ->result_array();

        $parents = $this->Menu_items
                    ->find()
                    ->where('menu_id', $row['menu_id'])
                    ->where_not_in('id', $id)
                    ->get()
                    ->result_array();

        $this->load->admin('menu_items/form', compact('menu', 'row', 'pages', 'parents'));
    }

    public function delete($id)
    {
        $row = $this->_load($id);

        $this->Menu_items->delete($id);

        $this->session->set_flashdata('success', 'Record deleted successfully.');
        redirect(admin_url('menuitems/index/' . $row['menu_id']));
    }

    public function hierarchy($id)
    {
        $menu = $this->_loadMenu($id);
        $this->pageTitle = 'Menu Items - ' . $menu['name'];

        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $data = json_decode($this->input->post('data'));

            $c1 = 1;
            foreach ($data as $d1)
            {
                $this->Menu_items->update(['parent_id' => null, 'sort_order' => $c1], $d1->id, false);

                $c1++;

                if(isset($d1->children))
                {
                    $c2 = 1;
                    foreach ($d1->children as $d2)
                    {
                        $this->Menu_items->update(['parent_id' => $d1->id, 'sort_order' => $c2], $d2->id, false);

                        $c2++;

                        if(isset($d2->children))
                        {
                            $c3 = 1;
                            foreach ($d2->children as $d3)
                            {
                                $this->Menu_items->update(['parent_id' => $d2->id, 'sort_order' => $c3], $d3->id, false);

                                $c3++;
                            }
                        }
                    }
                }
            }

            $this->session->set_flashdata('success', 'Hierarchy updated successfully.');
            redirect(admin_url('menuitems/index/' . $id));
        }

        $html = '';
        $menu = $this->Menu_items->find()
                                ->where('menu_id', $id)
                                ->where('(parent_id IS NULL OR parent_id = 0)')
                                ->order_by('sort_order', 'ASC')
                                ->get()
                                ->result_array();

        foreach ($menu as $m)
        {
            $html .= '<li class="dd-item" data-id="' .  $m['id'] . '"><div class="dd-handle">' . $m['label'] . '</div>';

            $childs1 = $this->Menu_items->find()
                                ->where('menu_id', $id)
                                ->where('parent_id', $m['id'])
                                ->order_by('sort_order', 'ASC')
                                ->get()
                                ->result_array();

            if($childs1)
            {
                $html .= '<ol class="dd-list">';

                foreach ($childs1 as $c1)
                {
                    $html .= '<li class="dd-item" data-id="' .  $c1['id'] . '"><div class="dd-handle">' . $c1['label'] . '</div>';

                    $childs2 = $this->Menu_items->find()
                                            ->where('menu_id', $id)
                                            ->where('parent_id', $c1['id'])
                                            ->order_by('sort_order', 'ASC')
                                            ->get()
                                            ->result_array();

                    if($childs2)
                    {
                        $html .= '<ol class="dd-list">';

                        foreach ($childs2 as $c2)
                        {
                            $html .= '<li class="dd-item" data-id="' .  $c2['id'] . '"><div class="dd-handle">' . $c2['label'] . '</div></li>';
                        }

                        $html .= '</ol>';
                    }

                    $html .= '</li>';
                }

                $html .= '</ol>';
            }

            $html .= '</li>';
        }

        $this->load->admin('menu_items/hierarchy', compact('html', 'id'));
    }

    public function datatable($id)
    {
        $model = $this->Menu_items->find()->where('menu_id', $id);
        $totalData = $totalFiltered = $this->Menu_items->count();
        $model = $this->Menu_items;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $order = 'mi.' . $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');
            
            foreach ($this->dataTableColumns as $c)
            {
                if (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(mi.' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = 'mi.' . $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';

            $model->setAlias('mi')
                ->find()
                ->where('mi.menu_id', $id)
                ->where($where)
                ->join('pages AS p', 'p.id = mi.page_id', 'left');

            $totalFiltered = $model->count();
        }

        $allData = $model->setAlias('mi')
                        ->find()
                        ->select('mi.*, p.title')
                        ->join('pages AS p', 'p.id = mi.page_id', 'left')
                        ->where('mi.menu_id', $id)
                        ->where($where)
                        ->limit($limit, $start)
                        ->order_by($order, $dir)
                        ->get()
                        ->result_array();

        $data = array();

        if(!empty($allData))
        {
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    if($c == 'page_id')
                    {
                        $row[] = $d[$c] ? $d['title'] : $d['link'];
                    }
                    elseif($c == 'status')
                    {
                        $row[] = $d[$c] ? 'Active' : 'Inactive';
                    }
                    elseif(in_array($c, $this->dateFields))
                    {
                        $row[] = date('d-M-Y h:ia', strtotime($d[$c]));
                    }
                    else
                    {
                        $row[] = $d[$c];    
                    }
                }

                $update = in_array('menu.update', $this->permissions) ? admin_url('menuitems/update/' . $d['id']) : '';
                $delete = in_array('menu.delete', $this->permissions) ? admin_url('menuitems/delete/' . $d['id']) : '';

                $actions = "<div class='btn-group'>";
                
                if($update)
                {
                    $actions .= "  <a href='{$update}' class='btn btn-primary btn-sm' title='Edit'><i class='fa fa-pencil'></i></a>";
                }

                if($delete)
                {
                    $actions .= "  <a href='{$delete}' class='btn btn-danger btn-sm' title='Delete' onclick='return confirm(\"Are you sure you want to delete this?\")'><i class='fa fa-trash'></i></button>";
                }

                $actions .= "</div>";

                $row[] = ($update || $delete) ? $actions : '';
                
                $data[] = $row;
            }
        }

        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->Menu_items
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('menu'));
        }

        return $row;
    }

    private function _loadMenu($id)
    {
        $row = $this->Menu
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            redirect(admin_url('menu'));
        }

        return $row;
    }

    private function _save(&$row = array(), $id = 0)
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->library('form_validation');

            $inputs = $this->input->post();
            $inputs['menu_id'] = $id;

            if ($this->Menu_items->validate($inputs))
            {
                if(isset($row['id']) && $row['id'])
                {
                    $this->Menu_items->update($inputs, $row['id'], false);
                }
                else
                {
                    $this->Menu_items->insert($inputs, false);
                }

                $this->session->set_flashdata('success', 'Record saved successfully.');
                redirect(admin_url('menuitems/index/' . $id));
            }
            else
            {
                $row = array_merge($row, $inputs);
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        return $row;
    }

    public function required_link($link)
    {
        $pid = $this->input->post('page_id');
        $link = $this->input->post('link');

        return $pid == 'custom' && $link == '' ? false : true;
    }
}
