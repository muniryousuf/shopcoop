<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sitevariables extends Admin_Controller {

    private $dataTableColumns = ["id","key","value","created_at"];
    private $dateFields = ["created_at","updated_at"];

    function __construct()
    {
        parent::__construct();

        $this->load->model('Site_variables_model', 'Site_variables');
        $this->pageTitle = 'Site Variables';
    }

    public function index()
    {
        $this->load->admin('site_variables/index');
    }

    public function create()
    {
        $row = $this->_save();

        $this->load->admin('site_variables/form', compact('row'));
    }

    public function update($id)
    {
        $row = $this->_load($id);

        $row = $this->_save($row);
        
        $this->load->admin('site_variables/form', compact('row'));
    }

    public function delete($id)
    {
        $row = $this->_load($id);

        $this->Site_variables->delete($id);

        $this->session->set_flashdata('success', 'Record deleted successfully.');
        redirect(admin_url('sitevariables'));
    }

    public function datatable()
    {
        $model = $this->Site_variables->find();
        $totalData = $totalFiltered = $this->Site_variables->count();
        $model = $this->Site_variables;

        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        $order = $this->dataTableColumns[$this->input->post('order[0][column]')];
        $dir = $this->input->post('order[0][dir]');

        $where = array();

        if(!empty($this->input->post('search[value]')))
        {
            $search = $this->input->post('search[value]');
            
            foreach ($this->dataTableColumns as $c)
            {
                if (in_array($c, $this->dateFields))
                {
                    $where[] = 'DATE_FORMAT(' . $c . ', "%d-%b-%Y %h:%i%p") LIKE "%' . $search . '%"';
                }
                else
                {
                    $where[] = $c . ' LIKE "%' . $search . '%"';
                }
            }

            $where = '(' . implode(' OR ', $where) . ')';
            $model->find()->where($where);

            $totalFiltered = $model->count();
        }

        $allData = $model->find()
                        ->where($where)
                        ->limit($limit, $start)
                        ->order_by($order, $dir)
                        ->get()
                        ->result_array();

        $data = array();

        if(!empty($allData))
        {
            foreach ($allData as $d)
            {
                $row = [];
                foreach($this->dataTableColumns as $c)
                {
                    $row[] = in_array($c, $this->dateFields) ? date('d-M-Y h:ia', strtotime($d[$c])) : $d[$c];
                }

                $update = in_array('sitevariables.update', $this->permissions) ? admin_url('sitevariables/update/' . $d['id']) : '';
                $delete = in_array('sitevariables.delete', $this->permissions) ? admin_url('sitevariables/delete/' . $d['id']) : '';

                $actions = "<div class='btn-group'>";
                
                if($update)
                {
                    $actions .= "  <a href='{$update}' class='btn btn-primary btn-sm' title='Edit'><i class='fa fa-pencil'></i></a>";
                }

                if($delete)
                {
                    $actions .= "  <a href='{$delete}' class='btn btn-danger btn-sm' title='Delete' onclick='return confirm(\"Are you sure you want to delete this?\")'><i class='fa fa-trash'></i></button>";
                }

                $actions .= "</div>";

                $row[] = ($update || $delete) ? $actions : '';
                
                $data[] = $row;
            }
        }

        $json_data = array(
            'draw'            => intval($this->input->post('draw')),
            'recordsTotal'    => intval($totalData),
            'recordsFiltered' => intval($totalFiltered),
            'data'            => $data
        );

        echo json_encode($json_data);
        exit;
    }

    private function _load($id)
    {
        $row = $this->Site_variables
                    ->find()
                    ->where('id', $id)
                    ->get()
                    ->row_array();

        if(!$row)
        {
            $this->session->set_flashdata('error', 'Record not found.');
            redirect(admin_url('sitevariables'));
        }

        return $row;
    }

    private function _save(&$row = array())
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $this->load->library('form_validation');

            $inputs = $this->input->post();

            if ($this->Site_variables->validate($inputs))
            {
                if(isset($row['id']) && $row['id'])
                {
                    $this->Site_variables->update($inputs, $row['id']);
                }
                else
                {
                    $this->Site_variables->insert($inputs);
                }

                $this->session->set_flashdata('success', 'Record saved successfully.');
                redirect(admin_url('sitevariables'));
            }
            else
            {
                $row = array_merge($row, $inputs);
                $this->session->set_flashdata('error', validation_errors());
            }
        }

        return $row;
    }
}
