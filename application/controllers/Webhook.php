<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webhook extends MY_Controller {

	public function mollie()
	{
		$this->load->model('Orders_model', 'Orders');
		$this->load->model('Order_address_model', 'OrderAddress');
		$this->load->model('Order_products_model', 'OrderProducts');

		$mollie = mollie_client();
		$sendcloud = sendcloud_client();

		try {
			$payment = $mollie->payments->get($_REQUEST["id"]);	
		} catch (Exception $e) {
			echo "API call failed: " . htmlspecialchars($e->getMessage());
			exit;
		}

		$order = $this->Orders
					->find()
					->where('payment_id', $payment->id)
					->get()
					->row_array();

		if($order)
		{
			if ($payment->isPaid())
			{
				$this->Orders->update(['status' => 1], $order['id'], false);

				$products = $this->OrderProducts
                                    ->find()
                                    ->where('order_id', $order['id'])
                                    ->get()
                                    ->result_array();

				$address = $this->OrderAddress
                                ->find()
                                ->where('order_id', $order['id'])
                                ->get()
                                ->result_array();

                $billing_addr = $shipping_addr = [];

                foreach ($address as $a)
                {
                	if($a['type'] == 1)
                	{
                		$billing_addr = $a;
                	}
                	elseif($a['type'] == 2)
                	{
                		$shipping_addr = $a;
                	}
                }

                if(!$shipping_addr)
                {
                	$shipping_addr = $address[0];
                }

                // Order Email - Customer
	            $data['title']    = 'Bedankt voor je bestelling';
	            $data['subject']  = "Je bestelling bij Pyjama's voor dames en kinderen - happysnooz.com is ontvangen!";
	            $data['order']    = $order;
	            $data['products'] = $products;
	            $data['billing']  = $billing_addr;
	            $data['shipping'] = $shipping_addr;
	            $email_body  = $this->load->front_email('order_customer', $data);

	            $this->email->from(NO_REPLY_EMAIL, site_name());
	            $this->email->to($order['email']);
	            $this->email->subject($data['subject']);
	            $this->email->message($email_body);
	            $this->email->send();

	            // Order Email - Admin
	            $data['title']    = 'Nieuwe bestelling: nr. ' . $order['id'];
	            $data['subject']  = "[" . site_title() . "]: Nieuwe bestelling #" . $order['id'];
	            $email_body = $this->load->front_email('order_admin', $data);

	            $this->email->from(NO_REPLY_EMAIL, site_name());
	            $this->email->to($this->vars['order_email']);
	            $this->email->subject($data['subject']);
	            $this->email->message($email_body);
	            $this->email->send();
				
                // Sendcloud
				$parcel = $sendcloud->parcels();
				$parcel->name 	      = $shipping_addr['first_name'] . ' ' . $shipping_addr['last_name'];
				$parcel->address      = substr($shipping_addr['address1'] . ' - ' . $shipping_addr['address2'], 0, 75);
				$parcel->city 		  = substr($shipping_addr['city'], 0, 30);
				$parcel->postal_code  = $shipping_addr['postalcode'] ;
				$parcel->country      = 'NL';
				$parcel->order_number = 'ORDER-' . $order['id'];
				$parcel->save();
			}
			elseif ($payment->isFailed() || $payment->isExpired())
			{
				$this->Orders->update(['status' => 4], $order['id'], false);
			}

			$this->load->library('pdf');
        	$this->pdf->order_invoice($order['id'], false);
        	$this->pdf->packing_slip($order['id'], false);
		}
	}
}