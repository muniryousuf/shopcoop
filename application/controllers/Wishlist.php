<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wishlist extends MY_Controller {

    public function add()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $inputs   = $this->input->post();
            $wishlist = $this->session->userdata('wishlist');
            $wishlist = $wishlist ? $wishlist : [];

            $id = $inputs['id'];
            
            $product = $this->Products
                            ->find()
                            ->where('id', $id)
                            ->get()
                            ->row_array();

            if($product)
            {
                $wishlist[$id] = $id;

                $this->session->set_userdata('wishlist', $wishlist);

                if($this->customer)
                {
                    $this->Customers->update(['wishlist' => json_encode($wishlist)], $this->customer['id'], false);
                }
            }

            echo count($wishlist);
            exit;
        }
        
        echo 0;
        exit;
    }

    public function remove()
    {
        if($this->input->server('REQUEST_METHOD') == 'POST')
        {
            $inputs   = $this->input->post();
            $wishlist = $this->session->userdata('wishlist');
            $wishlist = $wishlist ? $wishlist : [];

            $id = $inputs['id'];
            
            if(in_array($id, $wishlist))
            {
                unset($wishlist[$id]);
                $this->session->set_userdata('wishlist', $wishlist);

                if($this->customer)
                {
                    $this->Customers->update(['wishlist' => json_encode($wishlist)], $this->customer['id'], false);
                }
            }

            echo count($wishlist);
            exit;
        }
        
        echo 0;
        exit;
    }
}