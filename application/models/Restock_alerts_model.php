<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restock_alerts_model extends My_Model {

	const SOFT_DELETED = NULL;
	
	public $fillables = ["product_id","attributes","email","status"];

	public function rules()
    {
    	$rules = array(
			array(
				'field' => 'status',
				'label' => 'Status',
				'rules' => 'trim|required|numeric'
			),
			array(
				'field' => 'product_id',
				'label' => 'Product',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'trim|required'
			)
		);

		return $rules;
    }
}
