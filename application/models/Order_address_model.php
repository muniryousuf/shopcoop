<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Order_address_model extends My_Model {

	const CREATED_AT = NULL;
	const UPDATED_AT = NULL;
	const SOFT_DELETED = NULL;
	
	public $fillables = ["order_id","type","first_name","last_name","address1","address2","city","state","postalcode","country"];

	public function rules()
    {
    	$rules = array(
			array(
				'field' => 'order_id',
				'label' => 'Order Id',
				'rules' => 'trim|required|numeric'
			),
			array(
				'field' => 'type',
				'label' => 'Type',
				'rules' => 'trim|required|numeric'
			),
			array(
				'field' => 'first_name',
				'label' => 'First Name',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'last_name',
				'label' => 'Last Name',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'address1',
				'label' => 'Address',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'city',
				'label' => 'City',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'postalcode',
				'label' => 'Postalcode',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'country',
				'label' => 'Country',
				'rules' => 'trim|required'
			)
		);

		return $rules;
    }
}
