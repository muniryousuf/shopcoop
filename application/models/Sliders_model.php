<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sliders_model extends My_Model {

    public $fillables = ["name","slug","status"];

    public function rules()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'slug',
                'label' => 'Slug',
                'rules' => 'trim|required|callback_validate_slug',
                'errors' => array(
                    'validate_slug' => 'Slug already taken.'
                )
            )
        );

        return $rules;
    }

    public function slider_data($id){
        return $this->SliderDetails->find()
            ->where('slider_id = ' . $id[0])
            ->get()
            ->result_array();
    }
}
