<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_variables_model extends My_Model {

	public $fillables = ["key","value"];

	public function rules()
    {
    	$rules = array(
			array(
				'field' => 'key',
				'label' => 'Key',
				'rules' => 'trim|required'
			)
		);

		return $rules;
    }
}
