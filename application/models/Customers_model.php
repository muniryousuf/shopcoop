<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_model extends My_Model {

	public $is_new = 1;
	public $fillables = ["first_name","last_name","email","password","reset_key","wishlist"];

	public function rules()
    {
    	$rules = array(
			array(
				'field' => 'first_name',
				'label' => 'First Name',
				'rules' => 'trim|required'
			),
			array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'trim|required|callback_validate_unique_email',
				'errors' => array(
					'validate_unique_email' => 'Email address already taken.'
				)
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => $this->is_new ? 'trim|required' : 'trim'
			),
		);

		return $rules;
    }

    public function is_new($new = 1)
    {
    	$this->is_new = $new;
    }
}