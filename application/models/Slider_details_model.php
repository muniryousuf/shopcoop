<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slider_details_model extends My_Model {

    public $fillables = ["slider_id","image","alt_txt","text","sort","slider_url"];

}
