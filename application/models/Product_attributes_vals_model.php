<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_attributes_vals_model extends My_Model {

    const CREATED_AT = NULL;
    const UPDATED_AT = NULL;
    const SOFT_DELETED = NULL;
    
    public $fillables = ["name","attribute_id"];

    public function rules()
    {
        $rules = array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'attribute_id',
                'label' => 'Attribute',
                'rules' => 'trim|required'
            )
        );

        return $rules;
    }
}
